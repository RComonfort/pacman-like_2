// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Types.h"
#include "GhostController.generated.h"

/**
 * 
 */
UCLASS()
class MRGHOST_API AGhostController : public AAIController
{
	GENERATED_BODY()
	
#pragma region PROPERTIES

public:

private:


protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ghost AI")
	class UBehaviorTree* BT_Asset;


#pragma endregion PROPERTIES

#pragma region FUNCTIONS
private:


protected:


	virtual void OnPossess (class APawn* InPawn) override;


public:

	void SetBlackboardGhostState (EGhostState state);

#pragma endregion FUNCTIONS
};
