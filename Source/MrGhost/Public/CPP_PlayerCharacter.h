// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CPP_PlayerCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE (FPlayerDeathEvent);

UCLASS()
class MRGHOST_API ACPP_PlayerCharacter : public ACharacter
{
	GENERATED_BODY()

#pragma region PROPERTIES

public: 

	UPROPERTY(BlueprintAssignable, Category = "Player")
	FPlayerDeathEvent OnPlayerDeath;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UCameraComponent* Camera;

	UPROPERTY (VisibleAnywhere, Category = "Components")
	class USkeletalMeshComponent* GhostMesh;

	UPROPERTY (VisibleAnywhere, Category = "Components")
	class USpringArmComponent* SpringArm;

	UPROPERTY (VisibleAnywhere, BlueprintReadWrite, Category = "Powerup")
	bool bIsIndestructible = false;

private:

	float lastShotTime = 0;
	int32 powerupFXColorIndex = 0;
	float remainingDisguiseTime;
	FRotator initialRotOffest;

	class AGridActor* LevelGrid;
	class AAIDirector* AIDirector;
	class AMrGhostGameModeBase* gm;
	class APlayerController* controller;
	AActor* LevelCamera;

	FTimerHandle powerupTimerHandle;
	FTimerHandle trapTimerHandle;
	FTimerHandle powerupFXHandle;
	FTimerHandle abortJumpHandle;

	FVector jumpLandingLocation;

	bool bIsDead;
	bool bOnPlayerCamera = true;
	bool bIsDisguised = false;

	bool bChangedDirection = false;

	class UGridCell* currentGridCell;
	 
protected: 

	UPROPERTY (BlueprintReadWrite, Category = "Movement")
	bool bShouldMoveFwd;

	//Prevents player movement input or ability input when active
	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category = "Player")
	bool bActionLocked = false;

	UPROPERTY (BlueprintReadWrite, Category = "Movement")
	FVector MoveInput;

	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Shooting")
	bool bCanShoot = true;

	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Shooting")
	FName ProjectileFireSocket;

	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Shooting")
	TSubclassOf<class AProjectile> ProjectileClass;

	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Trap Setting")
	TSubclassOf<class ATrap> TrapClass;

	//Cooldown between shots (in secs)
	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Shooting")
	float ShootingCD = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PowerupFX")
	float PowerupFXTimePerColor = .5f;

	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "PowerupFX")
	TArray<FLinearColor> PowerupFXColors;

	UPROPERTY (EditDefaultsOnly, BlueprintReadOnly, Category = "PowerupFX")
	FName MatColorParamName = "Color";

	UPROPERTY(BlueprintReadOnly, Category = "PowerupFX")
	TArray<class UMaterialInstanceDynamic*> MeshDynMats;

	UPROPERTY (EditDefaultsOnly, BlueprintReadOnly, Category = "Disguise")
	float DisguiseTime = 5;

	//Minimum charge percent needed to activate the disguise
	UPROPERTY (EditDefaultsOnly, BlueprintReadOnly, Category = "Disguise")
	float MinDisguisePercent = .3;

	//Amount of grid cells player will move forward when jumping
	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Jumping")
	int32 JumpDistance = 3;

	//Time need for the character to abort unsuccessful jumps
	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Jumping")
	float JumpAbortTime = 1.5f;

	UPROPERTY (VisibleInstanceOnly, BlueprintReadOnly, Category = "Jumping")
	bool bIsJumping = false;

	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Camera Tracking")
	float MapCameraBlendTime = 1;
	
	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Camera Tracking")
	float MinArmLength = 200;

	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Camera Tracking")
	float MaxArmLength = 1500;

	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Camera Tracking")
	float ZoomSpeed = 100;

	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Camera Tracking")
	float MinPitch = -35.0f;

	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Camera Tracking")
	float MaxPitch = -80.0f;


#pragma endregion PROPERTIES

#pragma region FUNCTIONS
private: 

	FORCEINLINE void MovementTick (float DeltaTime);

	FORCEINLINE void DisguiseTick (float DeltaTime);

	FORCEINLINE void JumpingTick (float DeltaTime);

	UFUNCTION()
	void AttemptJump();

	UFUNCTION ()
	void TrapTimeEnded();

	UFUNCTION ()
	void PowerupEnded();

	/*
		Checks whether the player can move into a particular direction without colliding with geometry
		@param Direction Normalized world-space vector aligned with the X or Y axis. Indicates where player wishes to move
		@return True if the player would not collide in the immediate next grid cell in that direction. False otherwise
	*/
	bool MovementCheckCollision (const FVector& Direction);

	//Gets the level camera (camera actor tagged)
	void FindLevelCamera ();

	UFUNCTION()
	void SetNextColorOnPowerupFX ();

	float GetVerticalLaunchSpeed ();

	UFUNCTION ()
	void EndJump ();

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void PostInitializeComponents () override;

	UFUNCTION(BlueprintCallable, Category = "Movement")
	void MoveRight (float value);

	UFUNCTION (BlueprintCallable, Category = "Movement")
	void MoveUp (float value);

	UFUNCTION (BlueprintCallable, Category = "Shooting")
	void Shoot ();

	UFUNCTION (BlueprintCallable, Category = "Digging")
	void Dig ();

	UFUNCTION (BlueprintCallable, Category = "Damage")
	void Die ();

	UFUNCTION (BlueprintCallable, Category = "Disguising")
	void DisguiseToggle ();

	UFUNCTION (BlueprintCallable, Category = "Camera")
	void ViewChange (float BlendTime);
	
	UFUNCTION(BlueprintCallable, Category = "Camera")
	void ZoomChange (float value);

	UFUNCTION(BlueprintImplementableEvent, Category = "PowerupFX")
	void InterpolateMeshColors (const FLinearColor& from, const FLinearColor& to);

	UFUNCTION (BlueprintImplementableEvent, Category = "PowerupFX")
	void CancelMeshColorInterpolation ();

	void SetPlayerCollisionEnabled (bool enabled);
public:	

	// Sets default values for this character's properties
	ACPP_PlayerCharacter ();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable, Category = "Trapping")
	bool SteppedOnTrap (class ATrap* trap);

	/*
		Override the current move input so the player follows a particular direction. This is specially useful when teleporting it and wanting to follow the exit direction
		@param newInput The vector in the plane XY that dictates the new input, expected normalized and Axis-aligned
	*/
	UFUNCTION(BlueprintCallable, Category = "Movement")
	void OverrideMoveInput (const FVector newInput);

	/*
		Make the player invincible and capable of eating ghosts for the given duration
	*/
	UFUNCTION(BlueprintCallable, Category = "Powerup")
	void Powerup (float duration);

	UFUNCTION (BlueprintPure, BlueprintCallable, Category = "Disguise")
	float GetDisguisePercentAvailable () { return remainingDisguiseTime / DisguiseTime; }

	virtual float TakeDamage (float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	virtual void NotifyActorBeginOverlap (AActor* OtherActor) override;
#pragma endregion FUNCTIONS
};
