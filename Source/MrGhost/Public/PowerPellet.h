// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pellet.h"
#include "PowerPellet.generated.h"

class ACPP_PlayerCharacter;

/**
 * 
 */
UCLASS()
class MRGHOST_API APowerPellet : public APellet
{
	GENERATED_BODY ()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Power Pellet")
	float PowerupTime = 5;

protected:
	void OnConsumedByPlayer (ACPP_PlayerCharacter* player) override;

};
