
// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Types.h"
#include "Ghost.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE (FGhostReachTargetDest);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams (FGhostChangedState, EGhostState PrevState, EGhostState, NewState);

UCLASS()
class MRGHOST_API AGhost : public ACharacter
{
	GENERATED_BODY()

#pragma region PROPERTIES
public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Point Grant")
	float ScoreWhenEaten = 200;

	UPROPERTY (BlueprintAssignable, Category = "Movement")
	FGhostReachTargetDest OnReachedDestination;

	/*UPROPERTY (BlueprintAssignable, Category = "Ghost")
	FGhostChangedState OnChangedState;*/

	UPROPERTY (EditAnywhere, Category = "Debug")
	bool bDebugGhost = true;

protected:

	TArray<EGhostState> LastStates;

	/*Time the ghost will stay in its house after being eaten*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
	float TimeToHeal = 2.0f;

	/*Speed in cm/sec */
	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float DefaultMoveSpeed = 600;

	/*Name of the blackboard key corresponding to the target destination */
	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FName BlackboardDestinationKeyName = "MoveTargetDest";

	//If true, the ghost can be affected by the player traps
	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Traps")
	bool bCanBeTrapped = true;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Movement")
	class UGridCell* currentCell = nullptr;

	UPROPERTY (VisibleInstanceOnly, BlueprintReadOnly, Category = "Movement")
	UGridCell* previousCell = nullptr;

	UPROPERTY (VisibleInstanceOnly, BlueprintReadOnly, Category = "Movement")
	UGridCell* cellInFront = nullptr;

	//The states this ghost won't enter into
	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Ghost")
	TSet<EGhostState> forbiddenStates;

	//The state the ghost will enter when leaving the house
	UPROPERTY (EditDefaultsOnly, BlueprintReadOnly, Category = "Ghost")
	EGhostState DefaultEntryState = EGhostState::EGS_Chasing;

	UPROPERTY (VisibleInstanceOnly, BlueprintReadOnly, Category = "Movement")
	float SpeedMultiplier = 1;

	UPROPERTY (VisibleInstanceOnly, BlueprintReadOnly, Category = "Ghost")
	class AMrGhostGameModeBase* gm = nullptr;

	UPROPERTY (VisibleInstanceOnly, BlueprintReadOnly, Category = "Movement")
	bool bInGhostHouse = true;

	UPROPERTY (VisibleInstanceOnly, BlueprintReadWrite , Category = "Movement")
	FVector movingDirection;

	UPROPERTY (EditAnywhere, BlueprintReadOnly, Category = "Movement")
	bool bCustomMovementEnabled = true;

private:

	class AGhostController* GAIController = nullptr;
	class AGridActor* grid = nullptr;

	FTimerHandle leaveHouseHandle;
	FTimerHandle trapHandle;

	FVector targetDestination;


	FVector lastReachedDest;
	
	bool bCanLeaveHouse = false; 
	bool bDecidedIntersection = false;
	bool bCanMove = false;


#pragma endregion PROPERTIES

#pragma region FUNCTIONS
public:
	// Sets default values for this character's properties
	AGhost();

	// Called every frame
	virtual void Tick (float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent (class UInputComponent* PlayerInputComponent) override;

	virtual FVector GetVelocity () const override;

	void PossessedByController (AController* NewController);

	UFUNCTION (BlueprintCallable, Category = "Trap")
	bool IsTrapped () const;

	UFUNCTION (BlueprintCallable, Category = "Movement")
	void SetGhostMoveTarget (const FVector& target);

	UFUNCTION (BlueprintCallable, Category = "Ghost")
	void SetGhostToState (EGhostState state);

	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "Ghost")
	EGhostState GetGhostState ();

	//Sets timer for the ghost to leave ghost house and go into chase mode
	UFUNCTION(BlueprintCallable, Category = "Ghost")
	void SetTimerToLeaveHouse (float time, EGhostState intoState);

	/* Called when the ghost steps on a trap object*/
	UFUNCTION (BlueprintCallable, Category = "Trapping")
	bool SteppedOnTrap (class ATrap* trap);

	virtual void NotifyActorBeginOverlap (AActor* OtherActor) override;

	virtual float TakeDamage (float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION (BlueprintCallable, Category = "Movement")
	void EnteredOrExitedHouse (bool entered);

	UFUNCTION (BlueprintCallable, Category = "Movement")
	void ReverseMovingDirection ();

	//Adds a movement speed boost as a percent delta. Can be < 0 to reduce the speed boost
	UFUNCTION (BlueprintCallable, Category = "Movement")
	void GrantSpeedBoost (float multiplierDelta);


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent, meta=(DisplayName="OnEnteredNewCell"))
	void K2_OnEnteredNewCell();

	UFUNCTION (BlueprintImplementableEvent, meta = (DisplayName = "OnChangedState"))
	void K2_OnChangedState (EGhostState From, EGhostState To);

	UFUNCTION (BlueprintImplementableEvent, meta = (DisplayName = "OnEnteredHouse"))
	void K2_OnEnteredHouse (bool bEntered);

	UFUNCTION (BlueprintCallable, Category = "Movement")
	void LookAtMovingDirection ();

private:

	UFUNCTION ()
	void TrapTimeEnded ();

	UFUNCTION ()
	void EndHealing ();

	void SetLogicEnabled (bool enable, FString stopReason = FString());

	void UpdateTargetDestFromBB ();

	UGridCell* PickNewMovementCell (const TArray<UGridCell*>& cells, EGhostState inState);

	void MovementTick (float DeltaTime);


	/*Called when the ghost reaches its house and will wait until it heals*/
	void BeginHealing ();

	UFUNCTION ()
	void TimerToLeaveHouseEnded ();

	void DebugIntersection (TArray<UGridCell *>& neighbourCells);

	void RemoveInvalidNeighbours (EGhostState inState, TArray<UGridCell*>& neighbourCells);

#pragma endregion FUNCTIONS

};
