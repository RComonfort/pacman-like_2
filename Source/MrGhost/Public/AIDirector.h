// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Ghost.h"
#include "Types.h"
#include "AIDirector.generated.h"


UCLASS()
class MRGHOST_API AAIDirector : public AActor
{
	GENERATED_BODY()

#pragma region PROPERTIES

private:

	//Locks the ghosts to frightened mode while the flag is enabled
	bool bFrightenedLock = false;

	TSet<UGridCell*> CellsInGhostHouse;

public:
	
	//The settings for ghosts to spawn in the level
	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Ghost Spawning")
	TMap<TSubclassOf<AGhost>, FGhostSettings> GhostSettings;

	//Settings for waves 
	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Difficulty")
	TArray<FWaveSettings> Waves;

protected:

	//The box that defines the ghost house or safe zone
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Ghost Spawning")
	class UBoxComponent* GhostHouseVolume;

	//Ghosts instances that were spawned in the level
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Ghosts")
	TArray<AGhost*> GhostsInPlay;

	//The index of the current wave
	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category = "Difficulty")
	int CurrentWaveIndex = 0;

private:

	//Scatter phase comes first in wave. Except when player spawning, ghosts should leave house at their own time
	EGhostState waveState;
	FTimerHandle waveTimer;

	class AGridActor* grid;

#pragma endregion PROPERTIES

#pragma region FUNCTIONS

public:
	// Sets default values for this actor's properties
	AAIDirector ();

	// Called every frame
	virtual void Tick (float DeltaTime) override;

	/* Restarts the round by destroying all ghosts to respawn them again*/
	void RestartRound ();

	/*
		Called by player when he acquires or loses the powerup
		@param enabled True when the player obtains the powerup, false when it expires
	*/
	UFUNCTION(BlueprintCallable, Category = "AI Director")
	void PlayerPowerupEnabled (bool enabled);

	/*
		Returns the ghost actor instance, if it was spawned in the level.
		@param GhostClass The required ghost class
		@return Pointer to instance of ghost if available, null otherwise. 
	*/
	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "AI Director")
	AGhost* GetGhostInPlay (TSubclassOf<AGhost> GhostClass);

	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "Ghost House")
	TSet<UGridCell*> GetCellsInGhostHouse ();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay () override;

	UFUNCTION(BlueprintCallable)
	void SetAllGhostsToState (EGhostState state);

	void InitiateRound ();

private:

	void SetCellsInGhostHouse ();

	void SpawnGhosts ();

	void SetNextWave ();

	UFUNCTION()
	void WaveEndCallback ();

#pragma endregion FUNCTIONS


};

