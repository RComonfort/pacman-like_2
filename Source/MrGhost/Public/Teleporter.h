// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Teleporter.generated.h"

UCLASS()
class MRGHOST_API ATeleporter : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATeleporter();

	// Called every frame
	virtual void Tick (float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Teleporter")
	class UBoxComponent* TeleporterSideA;

	UPROPERTY (VisibleAnywhere, BlueprintReadOnly, Category = "Teleporter")
	UBoxComponent* TeleporterSideB;

	/*The direction that actors coming out from teleporter A will be facing*/
	UPROPERTY (VisibleAnywhere, BlueprintReadOnly, Category = "Teleporter")
	class UArrowComponent* AExitDir;

	/*The direction that actors coming out from teleporter B will be facing*/
	UPROPERTY (VisibleAnywhere, BlueprintReadOnly, Category = "Teleporter")
	UArrowComponent* BExitDir;

	UFUNCTION()
	void OnTeleporterBeginOverlap (class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION ()
	void OnTeleporterEndOverlap (class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
private:

	/*Set that holds actor that need to exit Teleporter A before they can be considered to teleport again*/
	TSet<AActor*> WaitForExitA;

	/*Set that holds actor that need to exit Teleporter B before they can be considered to teleport again*/
	TSet<AActor*> WaitForExitB;

};
