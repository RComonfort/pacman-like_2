// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "StaticHelpers.generated.h"

/**
 * 
 */
UCLASS()
class MRGHOST_API UStaticHelpers : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	
	/*
		Returns a normalized vector, that is aligned with the X or Y axis based on what is closer to the provided vector
		@param v Normalized target vector
		@result The axis vector that is closest to V
	*/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "StaticHelpers")
	static FVector GetClosestAxisFromVector (const FVector& v);

	/*
		Returns the grid actor that is in the current level (if any).
		@param WorldContextObject The object that made the request and is in the target Level
	*/
	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "StaticHelpers")
	static class AGridActor* GetLevelGrid (const UObject* WorldContextObject);

	/*
		Returns the AI director in the level (if any)
		@param WorldContextObject The object that made the request and is in the target Level
	*/
	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "StaticHelpers")
	static class AAIDirector* GetAIDirector (const UObject* WorldContextObject);
};
