// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "CPP_GameInstance.generated.h"

/**
 * 
 */
UCLASS()
class MRGHOST_API UCPP_GameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	UCPP_GameInstance ();

	UPROPERTY (EditDefaultsOnly, BlueprintReadWrite, Category = "Difficulty")
	int32 StartingLives = 3;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Levels")
	TArray<FName> LevelNames;

	UFUNCTION(BlueprintCallable, Category = "Levels")
	bool OpenNextLevel (float score, int32 lives);

	UFUNCTION (BlueprintCallable, Category = "Levels")
	bool OpenFirstLevel ();

	UFUNCTION (BlueprintCallable, Category = "Levels")
	void ParseOptionsString (const FString& optionsStr, float& outScore, int32& outLives);

protected:
	UFUNCTION(BlueprintCallable, Category = "Levels")
	FString GetDefaultOptionsString () { return FString::Printf (TEXT ("%f,%d"), 0, StartingLives); }

private:
	int32 CurrentLevelIndex;
};
