// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

UCLASS()
class MRGHOST_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();

	UPROPERTY (VisibleAnywhere, BlueprintReadOnly, Category = "Projectile")
	class UProjectileMovementComponent* MoveComp;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
	float ImpactDamage = 100;

	UPROPERTY(VisibleAnywhere, Category = "Projectile")
	UStaticMeshComponent* Mesh;

	//Actors in set will be ignore and no damage will be applied to them
	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Damage")
	TSet<AActor*> IgnoreActors;

	UPROPERTY (VisibleAnywhere, BlueprintReadOnly, Category = "Projectile")
	bool bDestroyed = false;

private:


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddIgnoredActor (AActor* actor);

	UFUNCTION ()
	void OnBeginOverlap (UPrimitiveComponent* OverlappedComponent,
					   AActor* OtherActor,
					   UPrimitiveComponent* OtherComp,
					   int32 OtherBodyIndex,
					   bool bFromSweep,
					   const FHitResult &SweepResult);
};
