// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Trap.generated.h"

UCLASS()
class MRGHOST_API ATrap : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATrap();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Trap")
	UStaticMeshComponent* Mesh;

	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Trap")
	float TrappedTime = 2;


	void Destroyed () override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION ()
	void OnBeginOverlap (UPrimitiveComponent* OverlappedComponent,
							 AActor* OtherActor,
							 UPrimitiveComponent* OtherComp,
							 int32 OtherBodyIndex,
							 bool bFromSweep,
							 const FHitResult &SweepResult);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Trap")
	bool bConsumed = false;

private:
	
	bool bHasCollidedWithPlayer = false;
	
	class UGridCell* GridCell;
};
