// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Types.h"
#include "GameFramework/Actor.h"
#include "PowerPellet.h"
#include "GhostWall.h"
#include "GridActor.generated.h"

class AWallActor;
class APellet;
class UGridCell;

UCLASS()
class MRGHOST_API AGridActor : public AActor
{
	GENERATED_BODY()

#pragma region PROPERTIES

public:


protected: 

	UPROPERTY (EditAnywhere, Category = "Grid Debug")
	bool bDebugGrid = true;

	//Data table holding the contents and dimensions of the map to generate. 
	UPROPERTY (EditAnywhere, BlueprintReadOnly, Category = "Grid Generation")
	class UDataTable* LevelGridTable;

	//Half size of a cell in cm
	UPROPERTY (EditAnywhere, BlueprintReadOnly, Category = "Grid Generation")
	float GridCellExtent = 50;

	//Number of cells in the X axis (rows)
	UPROPERTY (VisibleInstanceOnly, BlueprintReadOnly, Category = "Grid Generation")
	int32 GridXSize = -1; 

	//Number of cells in the Y axis (columns)
	UPROPERTY (VisibleInstanceOnly, BlueprintReadOnly, Category = "Grid Generation")
	int32 GridYSize = -1;

	//Matrix of grid cells
	UPROPERTY(BlueprintReadOnly, Category = "Grid")
	TArray<FGridMatrixRow> Grid;

	//Actor template to place as wall
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grid Contents")
	TSubclassOf<AWallActor> WallActor;

	//Actor template to place as pellet
	UPROPERTY (EditAnywhere, BlueprintReadOnly, Category = "Grid Contents")
	TSubclassOf<APellet> PelletActor;

	//Actor template to place as power pellet
	UPROPERTY (EditAnywhere, BlueprintReadOnly, Category = "Grid Contents")
	TSubclassOf<APowerPellet> PowerupActor;

	//Actor template to place as ghost house door, that blocks entry for player but not for ghosts
	UPROPERTY (EditAnywhere, BlueprintReadOnly, Category = "Grid Contents")
	TSubclassOf<AGhostWall> GhostDoor;

private:

	//Array holding the cells that are marked as ghost exits from their house
	TArray<UGridCell*> GhostHouseExits;

	class AAIDirector* LevelAIDirector;

#pragma region PROPERTIES

#pragma region FUNCTIONS

public:
	// Sets default values for this actor's properties
	AGridActor ();

	// Called every frame
	virtual void Tick (float DeltaTime) override;

	virtual void PostInitializeComponents () override; 


	/*Gets the cell whose center is closest to the provided world location in the XY plane*/
	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "Grid")
	UGridCell* GetCellFromLocation (const FVector& location);

	/*
		Returns a cell that is an offset amount from the given cell
		@param cell The target cell
		@param offsetX The amount of cells to offset downwards (in -X axis). Can be negative, meaning it will take the offset in the X axis direction
		@param offsetY The amount of cells to offset rightwards (in Y axis). Can be negative, meaning it will take the offset in the -Y axis direction

		@return The target cell. If the provided offsets are off grid, the result is clamped to a valid cell
	*/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Grid")
	UGridCell* GetCellOffsetFromOtherCell (const UGridCell* cell, int offsetX, int offsetY);

	/*
		Returns a cell that is an offset amount from the given cell
		@param cell The non-null cell to use as reference for the offset
		@param offsetX The amount of cells to offset downwards (in -X axis). Can be negative, meaning it will take the offset in the X axis direction
		@param offsetY The amount of cells to offset rightwards (in Y axis). Can be negative, meaning it will take the offset in the -Y axis direction

		@return The target cell. If the provided offsets are off grid, the result will be null
	*/
	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "Grid")
	UGridCell* GetCellOffsetWithinGrid (const UGridCell* cell, int offsetX, int offsetY);

	/*
		Returns true if the provided cell is an intersection and false otherwise
		i.e. it has 2 or more intersecting paths or it is a dead end (meaning it needs to be an intersection so ghosts can turn around)
	*/
	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "Grid")
	bool IsIntersection (const UGridCell* cell);

	/*
		Gets all neighbour cells to the provided cell, that are walkable. It only considers the top, bottom, left and right cells
		@param cell The target cell
		@param outNeighbours The array to fill with the found neighbours
	*/
	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "Grid")
	void GetNeighbouringCells (const UGridCell* cell, TArray<UGridCell*>& outNeighbours);

	/*
		Returns the world direction (X or Y Axis aligned) where the adjacent cell is located in relation to the given cell
		@param cell The current or "traveling from" cell
		@param adjacent The target or "traveling to" cell that is a direct neighbour of the given cell
		@return the direction vector from cell to adjacent, as (x,y,0) normalized 
	*/
	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "Grid")
	FVector GetDirectionTowardsAdjacentCell (const UGridCell* cell, const UGridCell* adjacent);

	/*
		Counts the number of walkable grid cells that are next to the given cell (in top, bot, left and right directions)
	*/
	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "Grid")
	int32 CountWalkableAdjacentCells (const UGridCell* cell);

	/*
		Returns true if the contents of the cell allow a character to walk through it
	*/
	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "Grid")
	bool IsWalkable (const UGridCell* cell);

	/*
		Returns true if the cell is available for player to jump into it (aka, it's not a wall, ghost wall or part of the ghost house)
		@param cell The non-null cell to query
	*/
	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "Grid")
	bool IsJumpable (UGridCell* cell);

	/*
		Returns the extent the grid cells
	*/
	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "Grid")
	float GetCellExtent ();

	/*
		Spawns a pellet instance from the given class, in the given cell if its contents are marked as empty.
		It will mark the cell as containing a pellet or power pellet now if it succeeded at spawning it
	*/
	UFUNCTION (BlueprintCallable, Category = "Grid")
	bool SpawnPelletAt (UGridCell* cell, TSubclassOf<APellet> pelletClass);

	/*
		Returns a random cell that is marked as ghost house exit. Returns null if there are non in the map
	*/
	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "Grid")
	UGridCell* GetRandomHouseExit ();

	/*
		Returns all the cells that "overlap" the box volume defined by 2 points in worldspace. 
		@param lowerLeftCorner Lower Left corner of the box (in direction of -X, -Y axis). Must be <= y and <= x to upper right corner
		@param upperRightCorner Upper right corner of the box (in direction of X, Y axis). Must be >= y and >= x to lower left corner
		@return An array containing pointers to all matching cells without duplicates
	*/
	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "Grid")
	TSet<UGridCell*> GetCellsWithinVolume (FVector lowerLeftCorner, FVector upperRightCorner);

	/*
		Evaluates whether the given location is nearly at (up to error tolerance) the location of the grid cell (considering XY coordinates only)
		@param cell The not-null ptr to a cell that we want to test against
		@param location The point in world space
		@param tolerance The epsilon or margin of error 
		@return If the 2 points are nearly equal 
	*/
	//UFUNCTION (BlueprintCallable, BlueprintPure, Category = "Grid", meta = (AutoCreateRefTerm = "tolerance"))
	bool IsLocationAtCell (const UGridCell* cell, const FVector& location, float tolerance = 1);

	/*
		Gets the Manhattan distance (in amount of cells) between 2 cells
		@param A Not-null pointer to the first cell
		@param B Not-null pointer to the second cell
		@result The Manhattan distance in amount of cells
	*/
	UFUNCTION (BlueprintCallable, BlueprintPure, Category = "Grid")
	int32 GetDistanceBetweenCells (const UGridCell* A, const UGridCell* B);


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay () override;


private:

	void GenerateGrid ();

	void DrawDebugGrid ();

	void SpawnActorFromCellContent (UGridCell& cell);

#pragma region FUNCTIONS

};
