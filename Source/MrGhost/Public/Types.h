// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Types.generated.h"

//Enum that expresses what a grid cell contains inside it
UENUM (BlueprintType)
enum class EGridCellContents : uint8
{
	EGCC_Empty,
	EGCC_Wall,
	EGCC_Pellet,
	EGCC_Powerup,
	EGCC_Hole,
	EGCC_GhostDoor,
	EGCC_GhostExit
};

//Enum that indicates in what state is a ghost in
UENUM (BlueprintType)
enum class EGhostState : uint8
{
	EGS_Waiting,
	EGS_Chasing,
	EGS_Scattering,
	EGS_Frightened,
	EGS_Healing,
	EGS_Stuck,
	EGS_Wandering
};

//Class that represents a cell inside a grid
UCLASS(Blueprintable)
class MRGHOST_API UGridCell : public UObject
{
	GENERATED_BODY ()

public:
	UPROPERTY (BlueprintReadWrite)
	FVector Location;

	UPROPERTY (BlueprintReadWrite)
	EGridCellContents CellContents;

	UPROPERTY (BlueprintReadWrite)
	int RowInGrid;

	UPROPERTY (BlueprintReadWrite)
	int ColInGrid;

};

//Struct to hold a TArray of grid cells. Used to create a row in a matrix of GridCells
USTRUCT (BlueprintType)
struct FGridMatrixRow
{
	GENERATED_BODY ()

	FGridMatrixRow (uint32 size = 0) {

		for (uint32 i = 0; i < size; i++)
			Cells.Add (NewObject<UGridCell> ());
	};

	UPROPERTY (BlueprintReadWrite)
	TArray<UGridCell*> Cells;

	FORCEINLINE UGridCell& operator [](int32 i) {
		return*Cells[i];
	}
};

//Struct to hold array of EGridCellContents. Used by a data table to indicate how a level should be layed out
USTRUCT (BlueprintType)
struct FCellContentRow : public FTableRowBase
{
	GENERATED_BODY ()

	UPROPERTY (BlueprintReadWrite, EditAnywhere)
	TArray<EGridCellContents> CellContentsRow;

};

//Struct that sets each ghost's settings that are level-variable
USTRUCT (BlueprintType)
struct MRGHOST_API FGhostSettings
{
	GENERATED_BODY ()

	//The world positions of where each ghost should scatter to
	UPROPERTY (EditAnywhere, BlueprintReadWrite)
	AActor* ScatterLocation;

	//The time (in secs) this ghost takes to leave the house at beginning of round
	UPROPERTY (EditAnywhere, BlueprintReadWrite)
	float TimeToLeaveHouse = 2;

	//If true, the ghost wont be spawned at begin play
	UPROPERTY (EditAnywhere, BlueprintReadWrite)
	bool bSkipSpawn = false;
};

//Struct that dictates how ghosts are changing between chase and scatter
USTRUCT (BlueprintType)
struct MRGHOST_API FWaveSettings
{
	GENERATED_BODY ()

	//Time (in secs) that ghosts are going to be scattered during the wave
	UPROPERTY (EditAnywhere, BlueprintReadWrite)
	float ScatterTime = 7;

	//Time (in secs) that ghosts are going to be chasing during the wave
	UPROPERTY (EditAnywhere, BlueprintReadWrite)
	float ChaseTime = 20;
};

