// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Pellet.generated.h"

class UStaticMeshComponent; 

UCLASS()
class MRGHOST_API APellet : public AActor
{
	GENERATED_BODY()
	

#pragma region PROPERTIES

protected:
	

public: 

	UPROPERTY (VisibleAnywhere, Category = Mesh)
	UStaticMeshComponent* MeshComp;

	UPROPERTY (BlueprintReadOnly, Category = "Pellet")
	bool bGrabbed;

	UPROPERTY (EditAnywhere, Category = "Pellet Grant")
	int32 PelletAmountGrant = 1;

	UPROPERTY (EditAnywhere, Category = "Pellet Grant")
	float PointsAwarded = 100;

private:

	class UGridCell* GridCell;
	class AMrGhostGameModeBase* gm;

#pragma endregion PROPERTIES


#pragma region FUNCTIONS

public:
	// Sets default values for this actor's properties
	APellet ();

	// Called every frame
	virtual void Tick (float DeltaTime) override;
	
	virtual void NotifyActorBeginOverlap (AActor * OtherActor);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay () override;

	/* Called when the pellet has been consumed by the player and is pending destroy*/
	virtual void OnConsumedByPlayer (class ACPP_PlayerCharacter* player) {};

#pragma endregion FUNCTIONS



};
