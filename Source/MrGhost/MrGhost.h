// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"

#define EObjectTypeQuery_WORLD_STATIC EObjectTypeQuery::ObjectTypeQuery1
#define EObjectTypeQuery_WORLD_DYNAMIC EObjectTypeQuery::ObjectTypeQuery2
#define EObjectTypeQuery_PAWN EObjectTypeQuery::ObjectTypeQuery3
#define EObjectTypeQuery_PHYSICS_BODY EObjectTypeQuery::ObjectTypeQuery4
#define EObjectTypeQuery_VEHICLE EObjectTypeQuery::ObjectTypeQuery5
#define EObjectTypeQuery_DESTRUCTIBLE EObjectTypeQuery::ObjectTypeQuery6

#define EObjectTypeQuery_GHOST EObjectTypeQuery::ObjectTypeQuery7
#define EObjectTypeQuery_PROJECTILE EObjectTypeQuery::ObjectTypeQuery8
#define EObjectTypeQuery_PELLET EObjectTypeQuery::ObjectTypeQuery9

#define EObjectTypeQuery_WALL EObjectTypeQuery::ObjectTypeQuery10
#define EObjectTypeQuery_FLOOR EObjectTypeQuery::ObjectTypeQuery11

#define ECollisionChannel_GHOST ECC_GameTraceChannel1
#define ECollisionChannel_PROJECTILE ECC_GameTraceChannel2
#define ECollisionChannel_PELLET ECC_GameTraceChannel3
#define ECollisionChannel_WALL ECC_GameTraceChannel4
#define ECollisionChannel_FLOOR ECC_GameTraceChannel5

DECLARE_LOG_CATEGORY_EXTERN (GhostDebugCategory, Display, Display);
DECLARE_LOG_CATEGORY_EXTERN (GhostAIDebugCategory, Display, Display);

#define DEBUG_ENABLED 1

#define GETENUMTCHAR(etype, evalue) ( (FindObject<UEnum>(ANY_PACKAGE, TEXT(etype), true) != nullptr) ? *(FindObject<UEnum>(ANY_PACKAGE, TEXT(etype), true)->GetNameStringByValue((int64)evalue)) : *FString("Invalid - are you sure enum uses UENUM() macro?") )



void Print (const FString& str);

void PrintScreen (const FString& str);