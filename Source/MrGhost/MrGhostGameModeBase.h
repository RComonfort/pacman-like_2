// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MrGhostGameModeBase.generated.h"

class AGridActor;
class AAIDirector;

/**
 * 
 */
UCLASS()
class MRGHOST_API AMrGhostGameModeBase : public AGameModeBase
{
	GENERATED_BODY ()


#pragma region PROPERTIES

public:

	UPROPERTY(BlueprintReadOnly, Category = "GameModeBase")
	class ACPP_PlayerCharacter* Player;

	UPROPERTY(BlueprintReadWrite, Category = "GameModeBase")
	int32 NeededPellets = 0;

	UPROPERTY (VisibleAnywhere, BlueprintReadOnly, Category = "GameModeBase")
	int32 PelletsCollected = 0;

	UPROPERTY(BlueprintReadOnly, Category = "GameModeBase")
	float Score;

	UPROPERTY (BlueprintReadOnly, Category = "GameModeBase")
	int RemainingLives;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Round Control")
	float WaitTimeAfterPlayerDeath = 2;

protected:

	UPROPERTY(BlueprintReadWrite, Category = "UI")
	class UUserWidget* GameplayScreenWidget;

	UPROPERTY (EditDefaultsOnly, BlueprintReadOnly, Category = "UI")
	TSubclassOf<UUserWidget> GameOverWidget;

private: 

	bool bLevelOver = false;
	class UCPP_GameInstance* gameInst;

#pragma endregion PROPERTIES

#pragma region FUNCTIONS

public:

	UFUNCTION(BlueprintImplementableEvent, BlueprintPure, BlueprintCallable, Category = "GameModeBase")
	AGridActor* GetLevelGrid ();

	UFUNCTION (BlueprintImplementableEvent, BlueprintPure, BlueprintCallable, Category = "GameModeBase")
	AAIDirector* GetLevelAIDirector ();

	UFUNCTION (BlueprintCallable, Category = "GameModeBase")
	void AddScore (float amount);

	UFUNCTION (BlueprintCallable, Category = "Pellets")
	int32 AddPelletsCollected (int32 amount);

	UFUNCTION (BlueprintCallable, Category = "Pellets")
	int32 AddPelletsToCollect (int32 amount);

	virtual void InitGame (const FString & MapName, const FString & Options, FString & ErrorMessage) override {	Super::InitGame (MapName, Options, ErrorMessage);
	K2_InitGame (MapName, Options, ErrorMessage);}

	UFUNCTION(BlueprintImplementableEvent, Category = "GameModeBase", meta = (DisplayName = "InitGame"))
	void K2_InitGame (const FString & MapName, const FString & Options, FString & ErrorMessage);

private:

	UFUNCTION()
	void OnPlayerDeath ();

	void OnGameOver ();

	void SuscribeToPlayerPawnDeath ();

	void OnPlayerAteAllPellets ();

protected:

	virtual void BeginPlay () override;

	UFUNCTION()
	void RestartRound ();

	UFUNCTION (BlueprintImplementableEvent, BlueprintCallable, Category = "Round Control")
	void InitiateRoundCountdown ();

#pragma endregion FUNCTIONS
};
