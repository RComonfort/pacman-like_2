// Fill out your copyright notice in the Description page of Project Settings.


#include "MrGhostGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "CPP_PlayerCharacter.h"
#include "Pellet.h"
#include "AIDirector.h"
#include "Blueprint/UserWidget.h"
#include "TimerManager.h"
#include "CPP_GameInstance.h"



void AMrGhostGameModeBase::BeginPlay ()
{
	Super::BeginPlay ();

	gameInst = Cast<UCPP_GameInstance> (UGameplayStatics::GetGameInstance (this));
	gameInst->ParseOptionsString (OptionsString, Score, RemainingLives);

	SuscribeToPlayerPawnDeath ();
}

void AMrGhostGameModeBase::AddScore (float amount)
{
	if (bLevelOver)
		return;

	if (amount > 0)
		Score += amount;

	//TODO: check if should add 1Up
}

int32 AMrGhostGameModeBase::AddPelletsCollected (int32 amount)
{
	if (bLevelOver)
		return PelletsCollected;

	PelletsCollected = FMath::Clamp(PelletsCollected + amount, 0, NeededPellets);

	if (PelletsCollected == NeededPellets)
		OnPlayerAteAllPellets ();

	return PelletsCollected;
}

int32 AMrGhostGameModeBase::AddPelletsToCollect (int32 amount)
{
	NeededPellets += amount;

	return NeededPellets;
}


void AMrGhostGameModeBase::OnPlayerDeath ()
{
	//Game Over
	if (--RemainingLives <= 0)
	{
		OnGameOver ();
	}
	else //Restart round
	{
		FTimerHandle handle;
		GetWorldTimerManager ().SetTimer (handle, this, &AMrGhostGameModeBase::RestartRound, WaitTimeAfterPlayerDeath, false);
	}
}

void AMrGhostGameModeBase::RestartRound ()
{
	APawn* pawn = UGameplayStatics::GetPlayerPawn (this, 0);
	if (pawn && pawn->IsValidLowLevel ())
		pawn->Destroy ();


	//Make AI Director reset the ghosts
	AAIDirector* aiDir = GetLevelAIDirector ();
	if (aiDir)
		aiDir->RestartRound ();

	//spawn new player pawn
	APlayerController* playerController = UGameplayStatics::GetPlayerController (this, 0);
	RestartPlayer (playerController);

	//Rebind death delegate
	SuscribeToPlayerPawnDeath ();

	//initiate round countdown 
	InitiateRoundCountdown ();
}

void AMrGhostGameModeBase::OnGameOver ()
{
	bLevelOver = true;

	//Remove gameplay screen widget
	if (GameplayScreenWidget)
		GameplayScreenWidget->RemoveFromViewport ();

	//Add gameover widget
	if (GameOverWidget)
	{
		APlayerController* playerController = UGameplayStatics::GetPlayerController (this, 0);
		UUserWidget* gameOverUI = CreateWidget (playerController, GameOverWidget);

		if (gameOverUI)
			gameOverUI->AddToViewport ();

		playerController->bShowMouseCursor = true;
	}
		
}

void AMrGhostGameModeBase::SuscribeToPlayerPawnDeath ()
{
	Player = Cast<ACPP_PlayerCharacter> (UGameplayStatics::GetPlayerPawn (this, 0));
	if (Player)
	{
		Player->OnPlayerDeath.AddDynamic (this, &AMrGhostGameModeBase::OnPlayerDeath);
	}
}

void AMrGhostGameModeBase::OnPlayerAteAllPellets ()
{
	bLevelOver = true;

	//TODO: Next level animation

	bool success = gameInst->OpenNextLevel (Score, RemainingLives);
	//if (!success)
		//TODO: Add Winning GUI
		
}

