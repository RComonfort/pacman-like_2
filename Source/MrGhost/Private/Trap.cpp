// Fill out your copyright notice in the Description page of Project Settings.


#include "Trap.h"
#include "Ghost.h"
#include "Engine/World.h"
#include "CPP_PlayerCharacter.h"
#include "Components/StaticMeshComponent.h"
#include "GridActor.h"
#include "StaticHelpers.h"

// Sets default values
ATrap::ATrap()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent> (TEXT ("Mesh"));
	RootComponent = Mesh;
}

// Called when the game starts or when spawned
void ATrap::BeginPlay()
{
	Super::BeginPlay();
	
	Mesh->OnComponentBeginOverlap.AddDynamic (this, &ATrap::OnBeginOverlap);


	AGridActor* LevelGrid = UStaticHelpers::GetLevelGrid (this);
	if (LevelGrid)
	{
		GridCell = LevelGrid->GetCellFromLocation (GetActorLocation ());

		if (GridCell)
			GridCell->CellContents = EGridCellContents::EGCC_Hole;
	}

}

void ATrap::OnBeginOverlap (UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if (bConsumed)
		return;


	bool foundTrappableActor = false;

	ACPP_PlayerCharacter* player = Cast<ACPP_PlayerCharacter> (OtherActor);

	if (player && !player->bIsIndestructible)
	{
		//If first collision with player, ignore
		
		if (!bHasCollidedWithPlayer && GetGameTimeSinceCreation () < 1)
		{
			bHasCollidedWithPlayer = true;
			return;
		}
		
		foundTrappableActor = player->SteppedOnTrap (this);
	}
	
	AGhost* ghost = Cast<AGhost> (OtherActor);
	if (ghost)
	{
		foundTrappableActor = ghost->SteppedOnTrap (this);
		
	}


	if (foundTrappableActor)
	{
		bConsumed = true;
		SetLifeSpan (TrappedTime);

		OtherActor->SetActorLocation (GetActorLocation());
	}
}


void ATrap::Destroyed ()
{
	if(GridCell)
		GridCell->CellContents = EGridCellContents::EGCC_Empty;
}

