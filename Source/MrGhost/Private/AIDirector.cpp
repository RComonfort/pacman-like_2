// Fill out your copyright notice in the Description page of Project Settings.

#include "AIDirector.h"
#include "Engine/World.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "StaticHelpers.h"
#include "TimerManager.h"
#include "MrGhost.h"
#include "GridActor.h"


// Sets default values
AAIDirector::AAIDirector()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	GhostHouseVolume = CreateDefaultSubobject<UBoxComponent> (TEXT ("GhostSafeZone"));
	RootComponent = GhostHouseVolume;
}

// Called when the game starts or when spawned
void AAIDirector::BeginPlay()
{
	Super::BeginPlay();

	//Set Timer to the first wave
	if (!ensureAlwaysMsgf (Waves.Num () > 0, TEXT("ERROR: Specify at least 1 wave in AI Director settings"))) return;

	CurrentWaveIndex = 0;
	waveState = EGhostState::EGS_Chasing;

	grid = UStaticHelpers::GetLevelGrid (this);

	SetCellsInGhostHouse ();

	FTimerHandle handle;
	GetWorldTimerManager ().SetTimer (handle, this, &AAIDirector::WaveEndCallback, Waves[0].ChaseTime, false);

	InitiateRound ();
}

void AAIDirector::SetAllGhostsToState(EGhostState state)
{
	if (bFrightenedLock)
		return;

	Print (FString::Printf (TEXT ("AIDir setting ghosts to %s"), GETENUMTCHAR("EGhostState", state)));

	for (auto& ghost : GhostsInPlay)
	{
		ghost->SetGhostToState (state);
	}
}

void AAIDirector::InitiateRound ()
{
	SpawnGhosts ();

	SetAllGhostsToState (EGhostState::EGS_Waiting);
}

void AAIDirector::SpawnGhosts ()
{
	UWorld* world = GetWorld ();
	if (!grid)
		grid = UStaticHelpers::GetLevelGrid (this);


	for (auto& tpair : GhostSettings)
	{
		if (tpair.Value.bSkipSpawn)
			continue;

		TSubclassOf<AGhost> ghostClass = tpair.Key;

		FVector spawnLoc = grid->GetCellFromLocation(UKismetMathLibrary::RandomPointInBoundingBox (GetActorLocation(), GhostHouseVolume->GetScaledBoxExtent ()))->Location;

		AGhost* ghost = world->SpawnActor<AGhost> (ghostClass, spawnLoc, FRotator::ZeroRotator);
		GhostsInPlay.Add(ghost);

		float timeToLeave = tpair.Value.TimeToLeaveHouse;
		ghost->SetTimerToLeaveHouse (timeToLeave, EGhostState::EGS_Chasing);
	}
}

void AAIDirector::SetNextWave ()
{
	if (CurrentWaveIndex >= Waves.Num ())
		return;

	Print (FString::Printf (TEXT ("AIDir: Creating timer for %s at wave index %d"), GETENUMTCHAR ("EGhostState", waveState), CurrentWaveIndex));

	FWaveSettings wave = Waves[CurrentWaveIndex];
	float time = waveState == EGhostState::EGS_Chasing ? wave.ChaseTime : wave.ScatterTime;
	GetWorldTimerManager ().SetTimer (waveTimer, this, &AAIDirector::WaveEndCallback, time, false);

	//Activate current wave
	SetAllGhostsToState (waveState);
}

void AAIDirector::WaveEndCallback ()
{
	Print (FString::Printf (TEXT ("AIDir ended timer for %s on wave %d"), GETENUMTCHAR ("EGhostState", waveState), CurrentWaveIndex));

	if (waveState == EGhostState::EGS_Chasing) //If chasing, change to next wave
		CurrentWaveIndex++;

	//Always change phase of wave
	waveState = waveState == EGhostState::EGS_Scattering ? EGhostState::EGS_Chasing : EGhostState::EGS_Scattering;

	SetNextWave ();
}

// Called every frame
void AAIDirector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAIDirector::RestartRound ()
{
	for (AGhost* ghost : GhostsInPlay) {
		ghost->Destroy ();
	}

	GhostsInPlay.Empty ();

	InitiateRound ();
}

void AAIDirector::PlayerPowerupEnabled(bool enabled)
{
	if (!enabled)
	{
		bFrightenedLock = false;
		SetAllGhostsToState (EGhostState::EGS_Chasing);
	}
	else if (enabled)
	{
		SetAllGhostsToState (EGhostState::EGS_Frightened);
		bFrightenedLock = true;
	}
}

AGhost* AAIDirector::GetGhostInPlay(TSubclassOf<AGhost> GhostClass)
{
	if (!ensureAlways (GhostClass)) return nullptr;

	for (AGhost* ghost : GhostsInPlay) {
		if (ghost->GetClass() == GhostClass)
			return ghost;
	}

	return nullptr;
}

TSet<UGridCell*> AAIDirector::GetCellsInGhostHouse()
{
	return CellsInGhostHouse;
}

void AAIDirector::SetCellsInGhostHouse ()
{
	if (!ensureAlways (grid)) return;

	FVector extents = GhostHouseVolume->GetScaledBoxExtent ();
	FVector lowerLeftCorner = GhostHouseVolume->GetComponentLocation () - extents;
	FVector upperRightCorner = GhostHouseVolume->GetComponentLocation () + extents;

	CellsInGhostHouse = grid->GetCellsWithinVolume (lowerLeftCorner, upperRightCorner);
}

