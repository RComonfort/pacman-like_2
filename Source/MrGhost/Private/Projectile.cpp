 // Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/StaticMeshComponent.h"
#include "DrawDebugHelpers.h"
#include "Teleporter.h"
#include "MrGhost.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent> (TEXT ("Mesh"));
	RootComponent = Mesh;

	MoveComp = CreateDefaultSubobject<UProjectileMovementComponent> (TEXT ("MovementComp"));

}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	
	Mesh->OnComponentBeginOverlap.AddDynamic (this, &AProjectile::OnBeginOverlap);
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectile::AddIgnoredActor (AActor* actor)
{
	if (actor)
		IgnoreActors.Add (actor);
}

void AProjectile::OnBeginOverlap (UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if (bDestroyed || (OtherActor == GetOwner () && (GetWorld ()->GetTimeSeconds () - CreationTime) < 1.5))
		return;

	//Ignore collisions if already contacted a valid actor, if the other actor is to be ignored or if it is a teleporter
	if (bDestroyed || IgnoreActors.Contains (OtherActor) || Cast<ATeleporter>(OtherActor)) 
		return;

	bool success = OtherActor->TakeDamage (ImpactDamage, FDamageEvent (), GetInstigatorController (), this) > 0;

	if (!success)
		return;
	 
	bDestroyed = true;
	Destroy ();
}

