// Fill out your copyright notice in the Description page of Project Settings.


#include "Teleporter.h"
#include "Components/BoxComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/PrimitiveComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "CPP_PlayerCharacter.h"
#include "StaticHelpers.h"
#include "Projectile.h"
#include "GameFramework/ProjectileMovementComponent.h"

// Sets default values
ATeleporter::ATeleporter()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent> (TEXT ("Root"));

	TeleporterSideA = CreateDefaultSubobject<UBoxComponent> (TEXT ("Teleporter Side A"));
	TeleporterSideA->SetupAttachment (RootComponent);

	TeleporterSideB = CreateDefaultSubobject<UBoxComponent> (TEXT ("Teleporter Side B"));
	TeleporterSideB->SetupAttachment (RootComponent);

	AExitDir = CreateDefaultSubobject<UArrowComponent> (TEXT ("Side A Exit Dir"));
	AExitDir->SetupAttachment (TeleporterSideA);

	BExitDir = CreateDefaultSubobject<UArrowComponent> (TEXT ("Side B Exit Dir"));
	BExitDir->SetupAttachment (TeleporterSideB);
}

// Called when the game starts or when spawned
void ATeleporter::BeginPlay()
{
	Super::BeginPlay();
	
	TeleporterSideA->OnComponentBeginOverlap.AddDynamic (this, &ATeleporter::OnTeleporterBeginOverlap);
	TeleporterSideB->OnComponentBeginOverlap.AddDynamic (this, &ATeleporter::OnTeleporterBeginOverlap);

	TeleporterSideA->OnComponentEndOverlap.AddDynamic (this, &ATeleporter::OnTeleporterEndOverlap);
	TeleporterSideB->OnComponentEndOverlap.AddDynamic (this, &ATeleporter::OnTeleporterEndOverlap);
}

void ATeleporter::OnTeleporterBeginOverlap(class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UBoxComponent* ExitComponent;
	
	//If actor triggers TP_A
	if (OverlappedComponent == TeleporterSideA)
	{
		ExitComponent = TeleporterSideB;

		//If actor comes from TP_B, then do not teleport it back
		if (WaitForExitA.Contains (OtherActor))
			return;

		WaitForExitB.Add (OtherActor);
	}

	else //else TP_B
	{
		ExitComponent = TeleporterSideA;

		//If actor comes from TP_A, then do not teleport it back
		if (WaitForExitB.Contains (OtherActor))
			return;

		WaitForExitA.Add (OtherActor);
	}

	FVector ExitLocation = ExitComponent->GetComponentLocation ();
	FVector ExitDir = ExitComponent->GetChildComponent (0)->GetForwardVector();

	OtherActor->SetActorLocation (ExitLocation, false, nullptr, ETeleportType::TeleportPhysics);
	OtherActor->SetActorRotation (UKismetMathLibrary::MakeRotFromXZ (ExitDir, FVector::UpVector)); 


	//If we are teleporting the player...
	ACPP_PlayerCharacter* player = Cast<ACPP_PlayerCharacter> (OtherActor);
	if (player)
	{
		//Set its movement to follow the ExitDir
		FVector moveIn = UStaticHelpers::GetClosestAxisFromVector (ExitDir);
		player->OverrideMoveInput (moveIn);

		return;
	}

	//if it is a projectile...
	AProjectile* projectile = Cast<AProjectile> (OtherActor);
	if (projectile) 
	{
		float VelMagnitude = projectile->MoveComp->Velocity.Size ();
		projectile->MoveComp->SetVelocityInLocalSpace (FVector(1,0,0) * VelMagnitude);

		return;
	}

	//if any other actor, simply adjust the velocity
	float VelMagnitude = OtherActor->GetRootComponent ()->GetComponentVelocity ().Size ();
	OtherActor->GetRootComponent ()->ComponentVelocity = VelMagnitude * ExitDir;
}

void ATeleporter::OnTeleporterEndOverlap (class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	//If left TP_A
	if (OverlappedComponent == TeleporterSideA)
	{
		WaitForExitA.Remove (OtherActor);
	}
	else //else TP_B
	{
		WaitForExitB.Remove (OtherActor);
	}
}

// Called every frame
void ATeleporter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

