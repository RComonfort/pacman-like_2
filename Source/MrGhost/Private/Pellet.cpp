// Fill out your copyright notice in the Description page of Project Settings.


#include "Pellet.h"
#include "CPP_PlayerCharacter.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "StaticHelpers.h"
#include "GridActor.h"
#include "MrGhostGameModeBase.h"


// Sets default values
APellet::APellet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent> (TEXT ("MeshComp"));
	RootComponent = MeshComp;
}

// Called when the game starts or when spawned
void APellet::BeginPlay()
{
	Super::BeginPlay();
	
	AGridActor* grid = UStaticHelpers::GetLevelGrid (this);
	if (grid)
		GridCell = grid->GetCellFromLocation (GetActorLocation ());

	gm = Cast<AMrGhostGameModeBase> (UGameplayStatics::GetGameMode (this));
}

// Called every frame
void APellet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APellet::NotifyActorBeginOverlap (AActor * OtherActor)
{
	ACPP_PlayerCharacter* Player = Cast<ACPP_PlayerCharacter> (OtherActor);

	if (Player && !bGrabbed)
	{
		bGrabbed = true;

		//Add score and pellet count
		if (gm)
		{
			gm->AddPelletsCollected (PelletAmountGrant);
			gm->AddScore (PointsAwarded);
		}

		//Clear content flag when the pellet has being picked up
		if (GridCell)
			GridCell->CellContents = EGridCellContents::EGCC_Empty; 

		OnConsumedByPlayer (Player);

		Destroy ();
	}
}

