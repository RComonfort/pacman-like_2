// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_PlayerCharacter.h"
#include "Math/UnrealMathUtility.h"
#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/World.h"
#include "Components/SkeletalMeshComponent.h"
#include "Projectile.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMaterialLibrary.h"
#include "Trap.h"
#include "TimerManager.h"
#include "GridActor.h"
#include "StaticHelpers.h"
#include "Ghost.h"
#include "MrGhost.h"
#include "AIDirector.h"
#include "DrawDebugHelpers.h"
#include "Camera/CameraActor.h"
#include "Materials/MaterialInstanceDynamic.h"

// Sets default values
ACPP_PlayerCharacter::ACPP_PlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent> (TEXT ("SpringArm"));
	SpringArm->SetupAttachment (RootComponent);

	Camera = CreateDefaultSubobject<UCameraComponent> (TEXT ("Camera"));
	Camera->SetupAttachment (SpringArm);

	GhostMesh = CreateDefaultSubobject<USkeletalMeshComponent> (TEXT ("GhostMesh"));
	GhostMesh->SetupAttachment (RootComponent);

}

// Called every frame
void ACPP_PlayerCharacter::Tick (float DeltaTime)
{
	Super::Tick (DeltaTime);

	if (!ensureAlways (LevelGrid)) return;

	currentGridCell = LevelGrid->GetCellFromLocation (GetActorLocation ());

	MovementTick (DeltaTime);

	DisguiseTick (DeltaTime);

	JumpingTick (DeltaTime);
}

// Called when the game starts or when spawned
void ACPP_PlayerCharacter::BeginPlay ()
{
	Super::BeginPlay ();

	bShouldMoveFwd = true;

	FindLevelCamera ();

	controller = GetController<APlayerController> ();

	LevelGrid = UStaticHelpers::GetLevelGrid (this);
	AIDirector = UStaticHelpers::GetAIDirector (this);

	//Instantiate dynamic materials from mesh
	TArray <UMaterialInterface*> meshMats = GetMesh ()->GetMaterials ();

	for (int i = 0; i < meshMats.Num(); i++)
	{
		UMaterialInstanceDynamic* newMat = UKismetMaterialLibrary::CreateDynamicMaterialInstance (this, meshMats[i]);
		MeshDynMats.Add (newMat);

		GetMesh ()->SetMaterial (i, newMat);
	}

	remainingDisguiseTime = DisguiseTime;
}


void ACPP_PlayerCharacter::PostInitializeComponents ()
{
	Super::PostInitializeComponents ();

	initialRotOffest = SpringArm->GetComponentRotation ();
}

// Called to bind functionality to input
void ACPP_PlayerCharacter::SetupPlayerInputComponent (UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent (PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveUp", this, &ACPP_PlayerCharacter::MoveUp);
	PlayerInputComponent->BindAxis ("MoveRight", this, &ACPP_PlayerCharacter::MoveRight);
	PlayerInputComponent->BindAxis ("Zoom", this, &ACPP_PlayerCharacter::ZoomChange);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACPP_PlayerCharacter::AttemptJump);
	PlayerInputComponent->BindAction ("Dig", IE_Pressed, this, &ACPP_PlayerCharacter::Dig);
	PlayerInputComponent->BindAction ("Shoot", IE_Pressed, this, &ACPP_PlayerCharacter::Shoot);

	PlayerInputComponent->BindAction ("Disguise", IE_Pressed, this, &ACPP_PlayerCharacter::DisguiseToggle);

	//Bind input for view change (requires the time parameter)
	FInputActionBinding binding ("ViewChange", IE_Pressed);
	FInputActionHandlerSignature delegate;
	delegate.BindUFunction (this, FName("ViewChange"), MapCameraBlendTime);

	binding.ActionDelegate = delegate;

	InputComponent->AddActionBinding (binding);
	
}

void ACPP_PlayerCharacter::MoveRight (float value)
{
	if (FMath::Abs (value) > 0.3f && !bActionLocked)
	{
		FVector tempMoveInput;

		tempMoveInput.Y = 1 * FMath::Sign (value);
		tempMoveInput.X = 0;

		//Check that player can change direction without bumping into something
		if (MovementCheckCollision (tempMoveInput))
		{
			if (tempMoveInput != MoveInput)
				bChangedDirection = true;

			MoveInput = tempMoveInput;
		}
	}
}

void ACPP_PlayerCharacter::MoveUp (float value)
{
	if (FMath::Abs (value) > 0.3f && !bActionLocked)
	{
		FVector tempMoveInput;

		tempMoveInput.X = 1 * FMath::Sign (value);
		tempMoveInput.Y = 0;

		//Check that player can change direction without bumping into something
		if (MovementCheckCollision (tempMoveInput))
		{
			if (tempMoveInput != MoveInput)
				bChangedDirection = true;

			MoveInput = tempMoveInput;
		}
	}
}

void ACPP_PlayerCharacter::AttemptJump ()
{
	if (!bWasJumping && !bActionLocked)
	{
		FVector fwd = UStaticHelpers::GetClosestAxisFromVector (GetActorForwardVector ());

		//Prevent jumps where player is "mid turn"
		if (!fwd.Equals(GetActorForwardVector ()))
			return;

		
		int32 offsetX = (int32)fwd.X * JumpDistance * -1;
		int32 offsetY = (int32)fwd.Y * JumpDistance;

		UGridCell* jumpCell = LevelGrid->GetCellOffsetWithinGrid (currentGridCell, offsetX, offsetY);
		FColor drawColor;

		if (jumpCell && LevelGrid->IsJumpable(jumpCell))
		{
			float tossVel = GetVerticalLaunchSpeed();
			
			LaunchCharacter (FVector::UpVector * tossVel, false, false);
			
			//Disable collision and movement and set jumping true state
			SetPlayerCollisionEnabled (false);
			bActionLocked = bIsJumping = true;
			bShouldMoveFwd = false;

			jumpLandingLocation = jumpCell->Location;

			drawColor = FColor::Green;

			//Set jump abort as precaution if stuck
			GetWorldTimerManager ().SetTimer (abortJumpHandle, this, &ACPP_PlayerCharacter::EndJump, JumpAbortTime, false);
		}
		else {
			drawColor = FColor::Red;
		}

		//TODO: draw invalid/valid landing location mesh
		FVector loc = jumpCell ? jumpCell->Location : GetActorLocation () + GetActorForwardVector () * LevelGrid->GetCellExtent() * JumpDistance;

		DrawDebugBox (GetWorld (), loc + FVector::UpVector * 50, FVector::OneVector * 50, drawColor, false, 10);
		DrawDebugBox (GetWorld (), currentGridCell->Location + FVector::UpVector * 50, FVector::OneVector * 50, FColor::Blue , false, 10);
	}

	
}

void ACPP_PlayerCharacter::MovementTick (float DeltaTime)
{
	if (bShouldMoveFwd)
	{

		float displacement = GetVelocity ().Size () * DeltaTime;
		//If will change direction and are close enough to cell center
		if (bChangedDirection && LevelGrid->IsLocationAtCell(currentGridCell, GetActorLocation(), displacement))
		{
			bChangedDirection = false;

			//Adjust player to cell center
			FVector newLoc = currentGridCell->Location;
			newLoc.Z = GetActorLocation ().Z;
			SetActorLocation (newLoc);
		}

		AddMovementInput (MoveInput, 1.f);
	}
}

void ACPP_PlayerCharacter::DisguiseTick (float DeltaTime)
{
	if (bActionLocked)
		return;

	if (bIsDisguised)
	{
		remainingDisguiseTime = FMath::Clamp (remainingDisguiseTime - DeltaTime, 0.0f, DisguiseTime);

		if (remainingDisguiseTime == 0)
			DisguiseToggle (); 
	}
	else
	{
		remainingDisguiseTime = FMath::Clamp (remainingDisguiseTime + DeltaTime, 0.0f, DisguiseTime);
	}
}

void ACPP_PlayerCharacter::JumpingTick (float DeltaTime)
{
	if (!bIsJumping)
		return;

	//If reached jumping target
	if (FVector::Dist2D (jumpLandingLocation, GetActorLocation ()) < 50)
	{
		GetWorldTimerManager ().ClearTimer (abortJumpHandle);
		EndJump ();
	}
}

void ACPP_PlayerCharacter::EndJump ()
{
	//Re-enable collision
	SetPlayerCollisionEnabled (true);

	bIsJumping = bActionLocked = false;

	bShouldMoveFwd = true;
}

void ACPP_PlayerCharacter::Shoot ()
{
	if (bActionLocked || GetWorld ()->TimeSeconds < ShootingCD + lastShotTime)
		return;

	lastShotTime = GetWorld ()->TimeSeconds;

	FVector loc = GetMesh ()->GetSocketLocation (ProjectileFireSocket);

	FActorSpawnParameters spawnParams;
	spawnParams.Instigator = this;
	spawnParams.Owner = this;

	FRotator rot = UKismetMathLibrary::MakeRotFromXZ (GetActorForwardVector (), FVector::UpVector);
	
	AProjectile* projectile = GetWorld ()->SpawnActor<AProjectile> (ProjectileClass, loc, rot, spawnParams);
}

void ACPP_PlayerCharacter::Dig ()
{
	if (bActionLocked || !ensureAlwaysMsgf (LevelGrid, TEXT("ERROR: Level grid null")))
		return;

	//TODO play trap deploy montage

	FVector spawnLoc = LevelGrid->GetCellFromLocation (GetActorLocation ())->Location;
	spawnLoc.Z -= LevelGrid->GetCellExtent ();
	GetWorld ()->SpawnActor<ATrap> (TrapClass, spawnLoc, FRotator());
}

void ACPP_PlayerCharacter::Die ()
{
	bShouldMoveFwd = false;
	bIsDead = true;

	DisableInput (controller);

	//Clear possible trap timer
	GetWorldTimerManager ().ClearTimer (trapTimerHandle);

	//TODO: Play death montage

	//Notify  of death
	OnPlayerDeath.Broadcast ();
	
}

void ACPP_PlayerCharacter::ViewChange (float BlendTime)
{
	AActor* targetActor = bOnPlayerCamera ? LevelCamera : this;

	if (!controller)
		controller = GetController<APlayerController> ();

	controller->SetViewTargetWithBlend (targetActor, BlendTime, EViewTargetBlendFunction::VTBlend_EaseInOut, 2);

	bOnPlayerCamera = !bOnPlayerCamera;
}

void ACPP_PlayerCharacter::ZoomChange (float value)
{
	if (FMath::IsNearlyZero (value, 0.01f))
		return;

	float currArmLength = SpringArm->TargetArmLength;
	currArmLength += ZoomSpeed * value;

	SpringArm->TargetArmLength = FMath::Clamp (currArmLength, MinArmLength, MaxArmLength);

	float alpha = UKismetMathLibrary::NormalizeToRange (SpringArm->TargetArmLength, MinArmLength, MaxArmLength);
	float pitchTarget = UKismetMathLibrary::Lerp (MinPitch, MaxPitch, alpha);

	initialRotOffest.Pitch = pitchTarget;

	SpringArm->SetWorldRotation(initialRotOffest);
}

void ACPP_PlayerCharacter::SetPlayerCollisionEnabled (bool enabled)
{
	if (enabled)
	{
		GetCapsuleComponent ()->SetCollisionResponseToChannel (ECollisionChannel_WALL, ECollisionResponse::ECR_Block);
			
		GetMesh ()->SetCollisionEnabled (ECollisionEnabled::QueryOnly);
	}
	else {
		GetCapsuleComponent ()->SetCollisionResponseToChannel (ECollisionChannel_WALL, ECollisionResponse::ECR_Ignore);
		GetMesh ()->SetCollisionEnabled (ECollisionEnabled::NoCollision);
	}
	
}

bool ACPP_PlayerCharacter::SteppedOnTrap(class ATrap* trap)
{
	ensure (trap);

	bShouldMoveFwd = false;
	bActionLocked = true;
	GetWorldTimerManager ().SetTimer (trapTimerHandle, this, &ACPP_PlayerCharacter::TrapTimeEnded, trap->TrappedTime);

	//TODO: play trapped montage

	return true;
}

void ACPP_PlayerCharacter::OverrideMoveInput (const FVector newInput)
{
	MoveInput = newInput; 
	ConsumeMovementInputVector ();
}

void ACPP_PlayerCharacter::Powerup (float duration)
{
	//play mesh FX
	powerupFXColorIndex = 0;
	GetWorldTimerManager ().SetTimer (powerupFXHandle, this, &ACPP_PlayerCharacter::SetNextColorOnPowerupFX, PowerupFXTimePerColor, true, 0);

	bIsIndestructible = true;
	if (AIDirector)
		AIDirector->PlayerPowerupEnabled (true);

	//Set timer for powerup
	GetWorldTimerManager ().SetTimer (powerupTimerHandle, this, &ACPP_PlayerCharacter::PowerupEnded, duration);
}

float ACPP_PlayerCharacter::TakeDamage (float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (bIsIndestructible || bIsDisguised || bIsDead)
		return 0;

	//Kill player
	Die ();
	return 1;
}

void ACPP_PlayerCharacter::NotifyActorBeginOverlap (AActor* OtherActor)
{
	//Only deal damage if being powered up
	if (bIsIndestructible)
	{
		AGhost* ghost = Cast<AGhost> (OtherActor);

		FDamageEvent event;
		if (ghost)
			ghost->TakeDamage (1, event, GetController (), this);
	}
}

void ACPP_PlayerCharacter::TrapTimeEnded ()
{
	//TODO: stop trapped montage

	bShouldMoveFwd = true;
	bActionLocked = false;
}

void ACPP_PlayerCharacter::PowerupEnded ()
{
	//stop mesh FX
	GetWorldTimerManager ().ClearTimer (powerupFXHandle);
	CancelMeshColorInterpolation ();

	//Return mesh to normal color
	for (auto dynMat : MeshDynMats)
	{
		dynMat->SetVectorParameterValue (MatColorParamName, FLinearColor::White);
	}

	bIsIndestructible = false;
	if (AIDirector)
		AIDirector->PlayerPowerupEnabled (false);
}

bool ACPP_PlayerCharacter::MovementCheckCollision (const FVector& Direction)
{
	
	UGridCell* targetCell = LevelGrid->GetCellOffsetFromOtherCell (currentGridCell, Direction.X * -1, Direction.Y);

	return targetCell->CellContents != EGridCellContents::EGCC_Wall && targetCell->CellContents != EGridCellContents::EGCC_GhostDoor;
}

void ACPP_PlayerCharacter::FindLevelCamera ()
{
	TArray<AActor*> outActors;
	UGameplayStatics::GetAllActorsOfClassWithTag (this, ACameraActor::StaticClass(), FName ("LevelCamera"), outActors);

	ensureMsgf (outActors.Num() > 0, TEXT("There needs to be a camera actor tagged \"LevelCamera\" in the level"));

	LevelCamera = outActors[0];
}

void ACPP_PlayerCharacter::SetNextColorOnPowerupFX ()
{
	if (PowerupFXColors.Num () == 0)
		return;
	FLinearColor fromColor;

	if (powerupFXColorIndex == 0)
		fromColor = FLinearColor::White;
	else
	{
		int32 prevIndex = powerupFXColorIndex - 1 < 0 ? PowerupFXColors.Num () - 1 : powerupFXColorIndex - 1;
		fromColor = PowerupFXColors[prevIndex];
	}

	FLinearColor& toColor = PowerupFXColors[powerupFXColorIndex];
	InterpolateMeshColors (fromColor, toColor);

	powerupFXColorIndex = (powerupFXColorIndex + 1) % PowerupFXColors.Num ();
}

void ACPP_PlayerCharacter::DisguiseToggle ()
{
	float percent = remainingDisguiseTime / DisguiseTime;

	//If want to toggle on but there is not enough charge, abort
	if (bActionLocked || (!bIsDisguised && percent < MinDisguisePercent))
		return;


	bIsDisguised = !bIsDisguised;

	GetMesh ()->ToggleVisibility ();
	GhostMesh->ToggleVisibility ();
}

float ACPP_PlayerCharacter::GetVerticalLaunchSpeed ()
{
	float apexHeight = GetActorLocation ().Z + (GetCapsuleComponent ()->GetScaledCapsuleHalfHeight () * 2);
	float jumpMidpoint = (GetActorLocation () + (GetActorForwardVector() * JumpDistance * LevelGrid->GetCellExtent ())).Size2D();
	
	float horizontalSpeed = Cast<UCharacterMovementComponent> (GetMovementComponent ())->MaxWalkSpeed;
	return (2 * apexHeight * horizontalSpeed) / jumpMidpoint; //2h(V.x) / apexXPoint
}
