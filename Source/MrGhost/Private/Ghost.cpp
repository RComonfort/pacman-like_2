// Fill out your copyright notice in the Description page of Project Settings.


#include "Ghost.h"
#include "GhostController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "TimerManager.h"
#include "MrGhost.h"
#include "Trap.h"
#include "AIModule/Classes/BrainComponent.h"
#include "CPP_PlayerCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "StaticHelpers.h"
#include "MrGhostGameModeBase.h"
#include "GridActor.h"
#include "Components/CapsuleComponent.h"
#include "DrawDebugHelpers.h"


void AGhost::DebugIntersection (TArray<UGridCell *>& neighbourCells)
{
	if (!DEBUG_ENABLED || !bDebugGhost)
		return;

	UWorld* world = GetWorld ();

	if (previousCell)
	{
		DrawDebugString (world, previousCell->Location, "Previous", nullptr, FColor::Black, 5);
		DrawDebugBox (world, previousCell->Location, FVector::OneVector * 25.f, FColor::Red, false, 5);
	}

	DrawDebugString (world, currentCell->Location, "Current Cell", nullptr, FColor::Black, 5);
	DrawDebugBox (world, currentCell->Location, FVector::OneVector * 25.f, FColor::Yellow, false, 5);

	for (auto cell : neighbourCells) {
		DrawDebugString (world, cell->Location, "Neighbour", nullptr, FColor::Black, 5);
		DrawDebugBox (world, cell->Location, FVector::OneVector * 25.f, FColor::Blue, false, 5);
	}

	DrawDebugString (world, cellInFront->Location, "Moving into", nullptr, FColor::Black, 5);
	DrawDebugBox (world, cellInFront->Location, FVector::OneVector * 25.f, FColor::Green, false, 5);

	DrawDebugDirectionalArrow (world, currentCell->Location, currentCell->Location + movingDirection * 35, 40, FColor::Orange, false, 5, 11);
}

// Sets default values
AGhost::AGhost()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AGhost::BeginPlay()
{
	Super::BeginPlay();	

	gm = Cast<AMrGhostGameModeBase> (UGameplayStatics::GetGameMode (this));

	grid = UStaticHelpers::GetLevelGrid (this);
}

// Called every frame
void AGhost::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UpdateTargetDestFromBB ();

	if (!grid)
		grid = UStaticHelpers::GetLevelGrid (this);

	//When entered a new cell, update variables
	UGridCell* ghostCell = grid->GetCellFromLocation (GetActorLocation ());
	if (ghostCell != currentCell)
	{
		previousCell = currentCell;
		currentCell = ghostCell;
		cellInFront = grid->GetCellOffsetFromOtherCell (currentCell, movingDirection.X * -1, movingDirection.Y );

		bDecidedIntersection = false;

		K2_OnEnteredNewCell ();
	}

	MovementTick (DeltaTime);
}


void AGhost::MovementTick (float DeltaTime)
{
	EGhostState state = GetGhostState ();

	if (!bCanMove || !bCustomMovementEnabled)
		return;

	float displacementInFrame = GetVelocity ().Size () * DeltaTime;

	//If reached destination, notify
	if (grid->IsLocationAtCell(currentCell, targetDestination, displacementInFrame) && lastReachedDest != targetDestination)
	{
		Print(FString::Printf (TEXT ("%s Reached current destination %s"), *GetName(), *targetDestination.ToString ()));

		lastReachedDest = targetDestination;
		OnReachedDestination.Broadcast ();

		return;
	}

	//If at intersection, we have not decided for it and are close to its center
	if (grid->IsIntersection (currentCell) && !bDecidedIntersection && grid->IsLocationAtCell(currentCell, GetActorLocation(), displacementInFrame))
	{
		SetActorLocation (FVector(currentCell->Location.X, currentCell->Location.Y, GetActorLocation().Z)); //snap ghost to cell center

		//Get neighbouring cells 
		TArray<UGridCell*> neighbourCells;
		grid->GetNeighbouringCells (currentCell, neighbourCells);

		RemoveInvalidNeighbours (state, neighbourCells);

		//pick a new move direction
		cellInFront = PickNewMovementCell (neighbourCells, state); //override cell in front to be the one at the new direction
		movingDirection = grid->GetDirectionTowardsAdjacentCell (currentCell, cellInFront);

		DebugIntersection (neighbourCells);

		LookAtMovingDirection ();

		bDecidedIntersection = true; //flag to avoid turning on this cell again
	}

	//move forward
	FVector newLoc = GetActorLocation () + GetVelocity () * DeltaTime;
	SetActorLocation (newLoc, true);
}

void AGhost::RemoveInvalidNeighbours (EGhostState inState, TArray<UGridCell*>& neighbourCells)
{
	//Remove ghost door as possible neighbour unless we are in healing state
	neighbourCells = neighbourCells.FilterByPredicate ([inState](UGridCell* cell) {
		return cell->CellContents != EGridCellContents::EGCC_GhostDoor || inState == EGhostState::EGS_Healing;
	});

	//remove the cell we came from unless it is the only cell available
	if (neighbourCells.Num () > 1)
		neighbourCells.Remove (previousCell);
}

//Rotate character to match moving direction
void AGhost::LookAtMovingDirection ()
{
	if (!cellInFront)
		return;

	FRotator lookRot = UKismetMathLibrary::FindLookAtRotation (GetActorLocation (), cellInFront->Location);
	lookRot.Pitch = lookRot.Roll = 0;
	SetActorRotation (lookRot.Quaternion ());
}

// Called to bind functionality to input
void AGhost::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

FVector AGhost::GetVelocity () const
{
	if (IsTrapped ())
		return FVector::ZeroVector;

	FVector facingDir = movingDirection.Size () == 0 ? GetActorForwardVector () : movingDirection;
	return facingDir * DefaultMoveSpeed * SpeedMultiplier;
}

bool AGhost::IsTrapped () const
{
	return GetWorldTimerManager ().IsTimerActive (trapHandle);
}

void AGhost::SetGhostMoveTarget (const FVector& target)
{
	targetDestination = target;

	if (DEBUG_ENABLED && bDebugGhost)
	{
		DrawDebugSphere (GetWorld (), targetDestination, 50.f, 16, FColor::Green, false, 1, 10, 1);
		DrawDebugString (GetWorld (), targetDestination, FString::Printf(TEXT("%s Target"), *GetName()), nullptr, FColor::Black, 1, false);
	}
}

void AGhost::SetGhostToState (EGhostState state)
{
	if (!bCanLeaveHouse || forbiddenStates.Contains(state))
		return;

	Print (FString::Printf (TEXT ("%s being set to %s"), *GetName(), GETENUMTCHAR ("EGhostState", state)));

	switch (state)
	{
		case EGhostState::EGS_Healing:
			if (gm)
				gm->AddScore (ScoreWhenEaten);

			bCanLeaveHouse = false; //lock ghost state to heal so no other action can interrupt it until the ghost has regenerated 
			ReverseMovingDirection ();
			break;

		case EGhostState::EGS_Frightened:
			//TODO: Slow Ghost down
			//TODO: Change mat 
			ReverseMovingDirection ();
			break;
		default:
			break;
	}

	if (GAIController)
	{
		K2_OnChangedState (GetGhostState (), state);
		GAIController->SetBlackboardGhostState (state);
	}
}

EGhostState AGhost::GetGhostState ()
{
	if (GAIController && GAIController->GetBlackboardComponent ())
	{
		return (EGhostState)GAIController->GetBlackboardComponent ()->GetValueAsEnum ("GhostState");
	}

	return EGhostState::EGS_Waiting;
}

void AGhost::SetTimerToLeaveHouse(float time, EGhostState intoState)
{
	Print (FString::Printf (TEXT ("%s setting timer to leave house (%f sec)"), *GetName (), time));

	/*FTimerDelegate delegate;
	delegate.BindUFunction (this, FName ("SetGhostToState"), intoState);*/
	GetWorld ()->GetTimerManager ().SetTimer (leaveHouseHandle, this, &AGhost::TimerToLeaveHouseEnded, time, false);
}

void AGhost::TimerToLeaveHouseEnded ()
{
	bCanLeaveHouse = true; 
	SetGhostToState (DefaultEntryState);
}


void AGhost::PossessedByController (AController* NewController)
{
	GAIController = Cast<AGhostController> (NewController);

	Super::PossessedBy (NewController);
}

void AGhost::NotifyActorBeginOverlap (AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap (OtherActor);

	ACPP_PlayerCharacter* player = Cast<ACPP_PlayerCharacter> (OtherActor);
	if (player && GetGhostState() != EGhostState::EGS_Healing)
	{
		FDamageEvent event;
		player->TakeDamage (1, event, GetController(), this);
	}
}

float AGhost::TakeDamage (float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (!CanBeDamaged())
		return 0;
	

	//don't take damage if already eaten by player
	if (GetGhostState () == EGhostState::EGS_Healing)
		return 0;

	GetWorldTimerManager ().ClearTimer (trapHandle);
	LastStates.Empty ();
	SetLogicEnabled (true);

	SetGhostToState (EGhostState::EGS_Healing);
	return 1;
}


bool AGhost::SteppedOnTrap(class ATrap* trap)
{
	if (!bCanBeTrapped)
		return false;

	check (trap);

	SetLogicEnabled (false, FString::Printf (TEXT ("Stepped in trap for %f"), trap->TrappedTime));

	//TODO: play anim montage

	//Set release timer
	GetWorldTimerManager ().SetTimer (trapHandle, this, &AGhost::TrapTimeEnded, trap->TrappedTime);

	//Add current state to stack so we can go back to it after released
	LastStates.Add(GetGhostState());

	SetGhostToState (EGhostState::EGS_Stuck);
	return true;

}

void AGhost::TrapTimeEnded ()
{
	SetLogicEnabled (true);

	//TODO: stop anim montage

	SetGhostToState (LastStates.Pop(false));
}

void AGhost::EndHealing ()
{
	bCanLeaveHouse = true;
	SetGhostToState (DefaultEntryState);

	//TODO: change mat back to normal
}

void AGhost::SetLogicEnabled (bool enable, FString stopReason)
{
	if (GAIController)
	{
		UBrainComponent* brain = GAIController->GetBrainComponent ();

		if (brain)
		{
			if (enable)
				brain->RestartLogic ();
			else
			{
				brain->StopLogic (stopReason);
				GAIController->StopMovement ();
			}
		}
	}

	bCanMove = enable;

}

void AGhost::UpdateTargetDestFromBB ()
{
	if (!GAIController)
		return;

	UBlackboardComponent* bb = GAIController->GetBlackboardComponent ();
	if (bb)
	{
		targetDestination = bb->GetValueAsVector (BlackboardDestinationKeyName);
	
	}
}

UGridCell* AGhost::PickNewMovementCell(const TArray<UGridCell*>& cells, EGhostState inState)
{
	if (!ensureAlwaysMsgf (cells.Num () != 0, TEXT("ERROR: 0 Neighbouring cells"))) return nullptr;

	if (cells.Num () == 1)
		return cells[0];

	
	//If frightened, pick a random cell
	if (inState == EGhostState::EGS_Frightened || inState == EGhostState::EGS_Wandering)
	{
		return cells[FMath::Rand () % cells.Num ()];
	}

	//Otherwise, pick the closest to target
	float sqrMinDist = FLT_MAX;
	UGridCell* resultCell = cells[0];

	for (UGridCell* cell : cells) {

		float currentSqrDist = FVector::DistSquared2D (targetDestination, cell->Location);
		if (currentSqrDist < sqrMinDist) 
		{
			sqrMinDist = currentSqrDist;
			resultCell = cell;
		}
	}

	return resultCell;
}


void AGhost::BeginHealing ()
{
	FTimerHandle handle;
	GetWorldTimerManager ().SetTimer (handle, this, &AGhost::EndHealing, TimeToHeal, false);

	bCanLeaveHouse = false;
}

void AGhost::EnteredOrExitedHouse (bool entered)
{
	bInGhostHouse = entered;

	//if entered
	if (entered)
	{
		bCanMove = false;//don't move ghost inside house (uses BT AI move to instead)

		if (GetGhostState () == EGhostState::EGS_Healing)
			BeginHealing ();
	}
	else //if exited, set moving direction to a value
	{
		bCanMove = true;

		movingDirection = FVector (0, -1, 0);
		LookAtMovingDirection ();
	}

	K2_OnEnteredHouse (entered);
}

void AGhost::ReverseMovingDirection ()
{
	movingDirection *= -1; //invert direction

	UGridCell* temp = previousCell;
	previousCell = cellInFront;
	cellInFront = temp;

	bDecidedIntersection = false;

	LookAtMovingDirection ();
}

void AGhost::GrantSpeedBoost (float multiplierDelta)
{
	SpeedMultiplier += multiplierDelta;
}
