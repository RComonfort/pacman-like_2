// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerPellet.h"
#include "CPP_PlayerCharacter.h"

void APowerPellet::OnConsumedByPlayer(ACPP_PlayerCharacter* player)
{
	ensure (player);

	player->Powerup (PowerupTime);
}
