// Fill out your copyright notice in the Description page of Project Settings.


#include "GridActor.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "StaticHelpers.h"
#include "MrGhost.h"
#include "Engine/DataTable.h"
#include "Types.h"
#include "Engine/World.h"
#include "Pellet.h"
#include "WallActor.h"
#include "NavigationSystem.h"
#include "MrGhostGameModeBase.h"
#include "AIDirector.h"



// Sets default values
AGridActor::AGridActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void AGridActor::BeginPlay()
{
	Super::BeginPlay();

	UNavigationSystemV1* nav = Cast<UNavigationSystemV1> (GetWorld ()->GetNavigationSystem ());
	if (nav)
		nav->Build ();

	LevelAIDirector = UStaticHelpers::GetAIDirector (this);
}

// Called every frame
void AGridActor::Tick (float DeltaTime)
{
	Super::Tick (DeltaTime);

}

void AGridActor::PostInitializeComponents ()
{
	Super::PostInitializeComponents ();
	
	GenerateGrid ();
	DrawDebugGrid ();
}

void AGridActor::GenerateGrid ()
{
	if (!ensureAlways (LevelGridTable)) return;

	TArray<FName> rowNames = LevelGridTable->GetRowNames ();
	GridXSize = rowNames.Num ();

	//Create all needed grid cells and assign them their contents based on the LevelDataTable
	for (int row = 0; row < GridXSize; row++)
	{
		FCellContentRow* rowContents = LevelGridTable->FindRow<FCellContentRow> (rowNames[row], FString ());

		if (row == 0)
			GridYSize = rowContents->CellContentsRow.Num ();

		for (int col = 0; col < GridYSize; col++)
		{
			if (col == 0)
				Grid.Emplace (GridYSize);

			Grid[row][col].CellContents = rowContents->CellContentsRow[col];
		}
	}

	float gridXLen = GridCellExtent * 2 * GridXSize;
	float gridYLen = GridCellExtent * 2 * GridYSize;

	//Top left corner (actor location is in center of grid)
	FVector TopLeftCorner = GetActorLocation () + (FVector::ForwardVector * (gridXLen / 2 - GridCellExtent)) - (FVector::RightVector * (gridYLen / 2 - GridCellExtent));

	//Assign locations to grid cells
	for (int row = 0; row < GridXSize; row++)
	{
		for (int col = 0; col < GridYSize; col++)
		{
			//Current cell
			UGridCell& cell = Grid[row][col];
			cell.Location = TopLeftCorner - FVector::ForwardVector * (row * GridCellExtent * 2) + FVector::RightVector * (col * GridCellExtent * 2);
			cell.Location.Z = GridCellExtent;

			cell.RowInGrid = row;
			cell.ColInGrid = col;

			SpawnActorFromCellContent (cell);
		}
	}
}

void AGridActor::DrawDebugGrid ()
{
	if (!bDebugGrid)
		return;

	for (int row = 0; row < GridXSize; row++)
	{
		for (int col = 0; col < GridYSize; col++)
		{
			const UGridCell& cell = Grid[row][col];

			FColor color = cell.CellContents == EGridCellContents::EGCC_Empty ? FColor::White : FColor::Red;
			DrawDebugBox (GetWorld (), cell.Location, FVector (GridCellExtent, GridCellExtent, 25.f), FColor::Yellow, true, 10.f);

			DrawDebugString (GetWorld (), cell.Location, FString::Printf (TEXT("%d, %d"), cell.RowInGrid, cell.ColInGrid), nullptr, FColor::Black, 100, true, 10);
		}
	}
}

void AGridActor::SpawnActorFromCellContent(UGridCell& cell)
{
	UClass* spawnClass;
	switch (cell.CellContents)
	{
		case EGridCellContents::EGCC_Wall:
			spawnClass = WallActor;
			break;
		case EGridCellContents::EGCC_Pellet:
		case EGridCellContents::EGCC_Powerup:
		{
			TSubclassOf<APellet> spawnPelletClass = cell.CellContents == EGridCellContents::EGCC_Powerup ? PowerupActor : PelletActor;
			cell.CellContents = EGridCellContents::EGCC_Empty;
			SpawnPelletAt (&cell, spawnPelletClass);
			return;
		}
		case EGridCellContents::EGCC_GhostDoor:
			spawnClass = GhostDoor;
			break;

		case EGridCellContents::EGCC_GhostExit:
			GhostHouseExits.Add (const_cast<UGridCell*>(&cell));
			return;
		default:  //empty
			return;
	}
	
	GetWorld ()->SpawnActor<AActor> (spawnClass, cell.Location, FRotator (0), FActorSpawnParameters ());

}


UGridCell* AGridActor::GetCellFromLocation(const FVector& location)
{
	FVector loc = FVector (location.X, location.Y, GridCellExtent);

	//Vector from top left corner to target location
	FVector gridToPoint = loc - Grid[0][0].Location ;

	int rowsOffset = FMath::RoundToInt ( FMath::Abs(gridToPoint.X) / (GridCellExtent * 2));
	int colsOffset = FMath::RoundToInt ( FMath::Abs (gridToPoint.Y) / (GridCellExtent * 2));

	rowsOffset = FMath::Clamp (rowsOffset, 0, GridXSize - 1);
	colsOffset = FMath::Clamp (colsOffset, 0, GridYSize - 1);

	return &Grid[rowsOffset][colsOffset];
}

UGridCell* AGridActor::GetCellOffsetFromOtherCell(const UGridCell* cell, int offsetX, int offsetY)
{
	int targetRow = FMath::Clamp (cell->RowInGrid + offsetX, 0, GridXSize - 1);
	int targetCol = FMath::Clamp (cell->ColInGrid + offsetY, 0, GridYSize - 1);

	return &Grid[targetRow][targetCol];
}

UGridCell* AGridActor::GetCellOffsetWithinGrid (const UGridCell* cell, int offsetX, int offsetY)
{
	if ((cell->RowInGrid + offsetX) >= GridXSize || (cell->ColInGrid + offsetY) >= GridYSize || 
		(cell->RowInGrid + offsetX) < 0 || (cell->ColInGrid + offsetY) < 0)
		return nullptr;

	return GetCellOffsetFromOtherCell (cell, offsetX, offsetY);
}

bool AGridActor::IsIntersection(const UGridCell* cell)
{
	TArray<UGridCell*> adjacents;
	GetNeighbouringCells (cell, adjacents);

	int32 openAdjacentCells = adjacents.Num();

	int32 row = cell->RowInGrid;
	int32 col = cell->ColInGrid;

	if (openAdjacentCells == 2)
	{
		//If the only 2 open neighbours are on the same row or the same col, it is not an intersection
		if ((adjacents[0]->RowInGrid == adjacents[1]->RowInGrid) || ((adjacents[0]->ColInGrid == adjacents[1]->ColInGrid)))
			return false;

		return true;
	}
	else //cells with 1, 3 and 4 open adjacent cells are considered intersections
		return true;
}

void AGridActor::GetNeighbouringCells(const UGridCell* cell, TArray<UGridCell*>& outNeighbours)
{
	int32 row = cell->RowInGrid;
	int32 col = cell->ColInGrid;

	//top
	if (row > 1 && IsWalkable(&Grid[row - 1][col]))
		outNeighbours.Add (&Grid[row - 1][col]);

	//bottom
	if (row < GridXSize - 1 && IsWalkable (&Grid[row + 1][col]))
		outNeighbours.Add (&Grid[row + 1][col]);

	//left 
	if (col > 1 && IsWalkable (&Grid[row][col - 1]))
		outNeighbours.Add (&Grid[row][col - 1]);

	//right
	if (col < GridYSize - 1 && IsWalkable (&Grid[row][col + 1]))
		outNeighbours.Add (&Grid[row][col + 1]);
}

FVector AGridActor::GetDirectionTowardsAdjacentCell(const UGridCell* cell, const UGridCell* adjacent)
{
	if (!ensureAlwaysMsgf (cell && adjacent, TEXT("Called w/null"))) return FVector::ZeroVector;

	if (adjacent->RowInGrid < cell->RowInGrid) //above
		return FVector (1, 0, 0);
	else if (adjacent->RowInGrid > cell->RowInGrid) //under
		return FVector (-1, 0, 0);
	else if (adjacent->ColInGrid < cell->ColInGrid) //on left
		return FVector (0, -1, 0);
	else if (adjacent->ColInGrid > cell->ColInGrid) //on right
		return FVector (0, 1, 0);
	else
		return FVector::ZeroVector;
}

int32 AGridActor::CountWalkableAdjacentCells(const UGridCell* cell)
{
	int32 row = cell->RowInGrid;
	int32 col = cell->ColInGrid;

	int32 count = 0;

	//top
	if (row > 1 && IsWalkable (&Grid[row - 1][col]))
		count++;

	//bottom
	if (row < GridXSize - 1 && IsWalkable (&Grid[row + 1][col]))
		count++;

	//left 
	if (col > 1 && IsWalkable (&Grid[row][col - 1]))
		count++;

	//right
	if (col < GridYSize - 1 && IsWalkable (&Grid[row][col + 1]))
		count++;

	return count;
}

bool AGridActor::IsWalkable (const UGridCell* cell)
{
	return cell->CellContents != EGridCellContents::EGCC_Wall;
}

bool AGridActor::IsJumpable(UGridCell* cell)
{
	if (!ensureAlways (cell && LevelAIDirector)) return false;

	return cell->CellContents != EGridCellContents::EGCC_Wall &&
		cell->CellContents != EGridCellContents::EGCC_GhostDoor && 
		!(LevelAIDirector->GetCellsInGhostHouse().Contains(cell))
		;
}

float AGridActor::GetCellExtent ()
{
	return GridCellExtent;
}

bool AGridActor::SpawnPelletAt(UGridCell* cell, TSubclassOf<APellet> pelletClass)
{
	if (!cell || !pelletClass || cell->CellContents != EGridCellContents::EGCC_Empty)
		return false;

	GetWorld ()->SpawnActor<APellet> (pelletClass, cell->Location, FRotator (0), FActorSpawnParameters ());

	AMrGhostGameModeBase* gm = Cast<AMrGhostGameModeBase> (UGameplayStatics::GetGameMode (this));
	if (gm)
		gm->AddPelletsToCollect (pelletClass->GetDefaultObject<APellet>()->PelletAmountGrant);

	
	cell->CellContents = UKismetMathLibrary::ClassIsChildOf (pelletClass, PowerupActor) ? EGridCellContents::EGCC_Powerup : EGridCellContents::EGCC_Pellet;

	return true;
}

UGridCell* AGridActor::GetRandomHouseExit ()
{
	if (GhostHouseExits.Num () == 0) return nullptr; return GhostHouseExits[FMath::Rand () % GhostHouseExits.Num ()];
}

TSet<UGridCell*> AGridActor::GetCellsWithinVolume(FVector lowerLeftCorner, FVector upperRightCorner)
{
	TSet<UGridCell*> seen;

	UGridCell* lowerLeft = GetCellFromLocation (lowerLeftCorner);
	UGridCell* upperRight = GetCellFromLocation (upperRightCorner);

	for (int32 col = lowerLeft->ColInGrid; col <= upperRight->ColInGrid && col < GridYSize; col++) {
		
		for (int32 row = lowerLeft->RowInGrid; row <= upperRight->RowInGrid && row >= 0; row--)
		{
			UGridCell* cell = &Grid[col][row];

			if (!seen.Contains (cell)) {
				seen.Add (cell);
			}
		}
	}

	return seen;
}

bool AGridActor::IsLocationAtCell (const UGridCell* cell, const FVector& location, float tolerance /*= KINDA_SMALL_NUMBER*/)
{
	return FVector::Dist2D (location, cell->Location) <= tolerance;
}

int32 AGridActor::GetDistanceBetweenCells (const UGridCell* A, const UGridCell* B)
{
	return FMath::Abs (A->RowInGrid - B->RowInGrid) + FMath::Abs (A->ColInGrid - B->ColInGrid);
}

