// Fill out your copyright notice in the Description page of Project Settings.


#include "StaticHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "MrGhostGameModeBase.h"
#include "MrGhost.h"

FVector UStaticHelpers::GetClosestAxisFromVector (const FVector& v)
{
	FVector projection = FVector::VectorPlaneProject (v, FVector::UpVector);

	float dotWithX = FVector::DotProduct (projection, FVector::ForwardVector);

	float angle = FMath::RadiansToDegrees(FMath::Acos (dotWithX));

	angle *= projection.Y < 0 ? -1 : 1;

	if (angle >= -45 && angle < 45)
		return FVector (1, 0, 0);
	else if (angle >= 45 && angle < 135)
		return FVector (0, 1, 0);
	else if (angle >= 135 || angle < -135)
		return FVector (-1, 0, 0);
	else
		return FVector (0, -1, 0);
}

class AGridActor* UStaticHelpers::GetLevelGrid(const UObject* WorldContextObject)
{
	AMrGhostGameModeBase* GM = Cast<AMrGhostGameModeBase> (UGameplayStatics::GetGameMode (WorldContextObject));
	if (GM)
	{
		return GM->GetLevelGrid ();
	}

	return nullptr;
}

class AAIDirector* UStaticHelpers::GetAIDirector (const UObject* WorldContextObject)
{
	AMrGhostGameModeBase* GM = Cast<AMrGhostGameModeBase> (UGameplayStatics::GetGameMode (WorldContextObject));
	if (GM)
	{
		return GM->GetLevelAIDirector ();
	}

	return nullptr;
}

