// Fill out your copyright notice in the Description page of Project Settings.

#include "CPP_GameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetStringLibrary.h"


UCPP_GameInstance::UCPP_GameInstance ()
{
	CurrentLevelIndex = 0;
}

bool UCPP_GameInstance::OpenNextLevel(float score, int32 lives)
{
	if (CurrentLevelIndex == LevelNames.Num () - 1)
		return false;

	FName& levelName = LevelNames[++CurrentLevelIndex];
	UGameplayStatics::OpenLevel (this, levelName, true, FString::Printf (TEXT("%f,%d"), score, lives));

	return true;
}

bool UCPP_GameInstance::OpenFirstLevel()
{
	CurrentLevelIndex = 0;
	FName& levelName = LevelNames[0];
	UGameplayStatics::OpenLevel (this, levelName, true, GetDefaultOptionsString());

	return true;
}

void UCPP_GameInstance::ParseOptionsString (const FString& optionsStr, float& outScore, int32& outLives)
{
	//Get options string (or the default one in the case the given optionsStr is empty
	const FString& str = optionsStr.Equals ("") ? GetDefaultOptionsString () : optionsStr;

	//Load score and lives from options string
	int32 substrIndex = UKismetStringLibrary::FindSubstring (str, ",");
	outScore = FCString::Atof (*UKismetStringLibrary::GetSubstring (str, 0, substrIndex));
	outLives = FCString::Atoi (*UKismetStringLibrary::GetSubstring (str, substrIndex + 1, str.Len () - 1 - substrIndex));
}
