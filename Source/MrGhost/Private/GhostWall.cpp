// Fill out your copyright notice in the Description page of Project Settings.


#include "GhostWall.h"
#include "StaticHelpers.h"
#include "GridActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"

// Sets default values
AGhostWall::AGhostWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent> (TEXT ("Root"));

	Mesh = CreateDefaultSubobject<UStaticMeshComponent> (TEXT ("Mesh"));
	Mesh->SetupAttachment (RootComponent);
}

// Called when the game starts or when spawned
void AGhostWall::BeginPlay()
{
	Super::BeginPlay();
	
	FTimerHandle handle;
	GetWorldTimerManager ().SetTimer (handle, this, &AGhostWall::CorrectActorRotation, 0.1, false);
}

// Called every frame
void AGhostWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGhostWall::CorrectActorRotation ()
{
	//Make wall match correct rotation
	AGridActor* grid = UStaticHelpers::GetLevelGrid (this);
	if (grid)
	{
		const UGridCell* cell = grid->GetCellFromLocation (GetActorLocation ());

		const UGridCell* aboveCell = grid->GetCellOffsetFromOtherCell (cell, -1, 0);

		//If the above cell is not empty, assume we must be looking sideways
		if (aboveCell->CellContents != EGridCellContents::EGCC_GhostExit)
			SetActorRotation (UKismetMathLibrary::MakeRotFromXZ (FVector::RightVector, FVector::UpVector));
	}
}

