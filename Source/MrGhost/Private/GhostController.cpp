// Fill out your copyright notice in the Description page of Project Settings.


#include "GhostController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Types.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Ghost.h"


void AGhostController::OnPossess (APawn* InPawn)
{
	Super::OnPossess (InPawn);

	RunBehaviorTree (BT_Asset);

	if (Blackboard)
	{
		SetBlackboardGhostState (EGhostState::EGS_Waiting);

		if (InPawn)
			Blackboard->SetValueAsClass (TEXT ("GhostClass"), InPawn->GetClass ());
	}

	AGhost* pawn = Cast<AGhost> (InPawn);
	ensure (pawn);
	if (pawn)
		pawn->PossessedByController (this);
}

void AGhostController::SetBlackboardGhostState (EGhostState state)
{
	Blackboard->SetValueAsEnum (TEXT ("GhostState"), (uint8)state);
}
