// Fill out your copyright notice in the Description page of Project Settings.

#include "MrGhost.h"
#include "Modules/ModuleManager.h"
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MrGhost, "MrGhost" );

DEFINE_LOG_CATEGORY (GhostDebugCategory);
DEFINE_LOG_CATEGORY (GhostAIDebugCategory);

void Print(const FString& str)
{
	if (DEBUG_ENABLED)
		UE_LOG (GhostDebugCategory, Display, TEXT ("%s"), *str);
}

void PrintScreen(const FString& str)
{
	if (!GEngine)
		return;

	GEngine->AddOnScreenDebugMessage (INDEX_NONE, 2, FColor::Cyan, str, true);

}
